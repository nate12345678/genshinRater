# Describe the problem you are facing

## Link a copy of your input json

# Checklist

- [ ] Read the entire [readme](https://gitlab.com/nate12345678/genshinRater/-/blob/main/README.md)
- [ ] Checked other issues, no similar issues exist
- [ ] Confirmed this issue occurs using the latest version, with no custom config

