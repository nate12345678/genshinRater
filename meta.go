package genshinrater

import (
	_ "embed"
)

//go:embed config.json
var DefaultConfig []byte

//go:embed README.md
var Readme []byte
