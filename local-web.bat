@echo off
set "BUILD_DIR=.\public\"
set "LDFLAGS= -s -w"
set "GOOS=js"
set "GOARCH=wasm"

for /f %%i in ('git describe --tags --always') do set VERSION=%%i

go build -ldflags="%LDFLAGS% -X main.Version=%VERSION%" -o %BUILD_DIR% .\cmd\rater.wasm


del /S /q .\web
rd /s /q .\web
xcopy /s /i /y .\public .\web\genshinRater
md .\web\genshinRater\builds
md .\web\genshinRater\readme
.\build\rater.exe -html-config
.\build\rater.exe -html-readme
copy .\config.html .\web\genshinRater\builds\index.html
copy .\readme.html .\web\genshinRater\readme\index.html
xcopy /s /i /y .\assets .\web\genshinRater\assets

echo Serve the 'web' directory with a webserver (ex python3 -m http.server), and navigate to localhost:8000/genshinRater
