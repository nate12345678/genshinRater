import sys

def binary_search(arr, low, high, x):

    # Check base case
    if high >= low:

        mid = (high + low) // 2

        # If element is present at the middle itself
        if arr[mid] == x:
            return mid

        # If element is smaller than mid, then it can only
        # be present in left subarray
        elif arr[mid] > x:
            return binary_search(arr, low, mid - 1, x)

        # Else the element can only be present in right subarray
        else:
            return binary_search(arr, mid + 1, high, x)

    else:
        # Element is not present in the array
        return -1

def linear_search(arr, x):
    for item in arr:
        if(item == x):
            return 1
    return -1

def remove_extra_spaces(strval):
    for i in range(8):
        strval = strval.replace("  ", " ")
    return strval

def collapse_whitespace(text):
    towhitespace = "\t \n \r - / . , [ ] ( )".split(" ")
    for char in towhitespace:
        text = text.replace(char, ' ')
    text = remove_extra_spaces(text)
    return text

if __name__ == "__main__":
    dictname = "ci/dict.txt"
    maxfails = 0.02

    if(len(sys.argv) < 2):
        raise Exception("Needs a filename")

    # Read the text fle
    filename = sys.argv[1]
    textfile = open(sys.argv[1], 'r')
    text = textfile.read().lower()
    textfile.close()

    dictextratxt = []
    if(len(sys.argv) >= 3):
        dictextrafile = open(sys.argv[2], 'r')
        dictextratxt = dictextrafile.read().split('\n')
        dictextrafile.close()

    # Reads a dictionary file (one word per line) and creates an array
    dictfile = None
    try:
        dictfile = open(dictname, 'r')
    except:
        raise FileNotFoundError("This program should be shipped with dict.txt")
    dicttxt = dictfile.read().split('\n')
    dictfile.close()

    # Prep the text data
    print("Merging dictionaries")
    dicttxt = sorted(dicttxt + dictextratxt)

    print("Prepping text")
    text = collapse_whitespace(text) # Removes all special whitespace characters and collapses spaces to 1
    allowed = [" "] + "a b c d e f g h i j k l m n o p q r s t u v w x y z".split() # Sorted for binary search
    i = 0
    while(i < len(text)):
        if(binary_search(allowed, 0, len(allowed) - 1, text[i]) != -1): # match found
            i += 1
        else: # remove this char and all instances
            text = text.replace(text[i], '')
    text = remove_extra_spaces(text)
    print("Finding Misspells")
    words = text.split(" ")
    print("Word count: " + str(len(words)))
    failsallowed = int(len(words) * maxfails)
    misspells = " " + text + " " # Make sure first and last word can be detected
    for word in words:
        if(binary_search(dicttxt, 0, len(dicttxt), word) != -1):
            misspells = misspells.replace(" " + word + " ", " ")
    misspells = remove_extra_spaces(misspells)
    print("All found mispellings for " + filename + ":")
    print(misspells)
    cnt = len(misspells.split())
    print("Total: " + str(cnt))
    if(cnt > failsallowed):
        raise Exception("Too many misspelled words!")
