// nolint: mnd, nolintlint
package rater

// Constants for defining how much of a score a value is worth.

// Weapons.
const (
	weaponMaxLevel     = 90
	weaponMaxAscension = 6

	correctWeaponWeight = 1.0 / 3.0
	weaponLevelWeight   = 2.0 / 3.0
)

// Artifacts.
const (
	artifactMaxRarity = 5
	artifactMaxLevel  = 20

	artifactsPerCharacter  = 5
	artifactsWithMainStats = 3.0

	lowRVThreshold  = 640.0
	lowRVPercent    = 0.10
	lowSubThreshold = 270.0

	// weight of each level of an artifact.
	// weight of levels all together is 2/5, each level is 1/100 of that fraction.
	artifactLevelWeight = 1.0 / (artifactMaxLevel * artifactsPerCharacter) * (2.0 / 5.0)

	// weight of 3 correct mainstats.
	// total weight of mainstats is 2/5.
	artifactMainStatWeight = 2.0 / 5.0 / artifactsWithMainStats

	artifact4SetWeight = 1.0 / 5.0
	artifact2SetWeight = artifact4SetWeight / 3.0
)

// Characters.
const (
	characterMaxLevel = 90
	charMaxAscension  = 6
	talentMaxLevel    = 10
)

// Other.
const (
	numScoreCategories = 6.0
	exponentWeight     = 1.0
	maxCategoryMult    = exponentWeight + 1.0

	pctToInt = 100.0
)
