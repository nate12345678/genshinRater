package rater

import (
	"testing"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

func TestTalentLimit(t *testing.T) {
	t.Parallel()
	if talentLimit(99) != 10 {
		t.Fail()
	}
	if talentLimit(6) != 6 {
		t.Fail()
	}
}

func TestCharacterWantsSubstat(t *testing.T) {
	t.Parallel()
	c := models.DefaultConfig()
	c.AllowedSubs = []string{"def_", "atk_", "eleMas"}
	if characterWantsSubstat("hp_", c) {
		t.Fail()
	}
	if !characterWantsSubstat("eleMas", c) {
		t.Fail()
	}
}

func TestScoreTalents(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	c := models.Character{
		Key:           "Albedo",
		Level:         90,
		Constellation: 0,
		Ascension:     6,
		Talent: map[string]int{
			"auto":  2,
			"skill": 6,
			"burst": 10,
		},
	}
	cases := []map[string]int{
		{"auto": 2, "skill": 6, "burst": 10},
		{"auto": 1, "skill": 1, "burst": 1},
		{"auto": 10, "skill": 10, "burst": 10},
		{"auto": 6, "skill": 8, "burst": 10},
	}
	scores := []float32{1, 1, 0.775, 0.895}

	for i, v := range cases {
		s := scoreTalents(c, v)
		e := scores[i]
		if s != e {
			t.Logf("talent score: %f, expected: %f", s, e)
			t.Fail()
		}
	}
}
