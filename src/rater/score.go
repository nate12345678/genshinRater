package rater

import (
	"fmt"
	"math"
	"strings"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/format"
	"gitlab.com/nate12345678/genshinRater/src/log"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

func scoreCharacter(
	conf models.CharacterConfig, intm models.CharacterIntermediate, unusedWeap map[string]int,
) models.CharacterRating {
	rating := models.DefaultRating()
	char := intm.Character

	// Score level and constellation
	// mult from level + % of total mult from ascension / tot mult
	rating.LevelScore = format.Round(float32((data.LevelStatScale5Star[char.Level] +
		data.AscensionStatScale[char.Ascension]*data.AscensionStatMultiplier5Star) / data.LevelMultMax5Star))
	rating.ConstellationMet = char.Constellation >= conf.RecommendedConstellation
	if char.Level < characterMaxLevel {
		rating.Comments += "Character is not level 90\n"
	}
	if !rating.ConstellationMet {
		rating.Comments += fmt.Sprintf("Recommended Constellation is level %d\n", conf.RecommendedConstellation)
	}

	// Score and comment on talents
	rating.TalentScore = scoreTalents(char, conf.TalentPriority)
	if char.Talent["auto"] < conf.TalentPriority["auto"] {
		rating.Comments += fmt.Sprintf("Normal attack is below level %d\n", conf.TalentPriority["auto"])
	}
	if char.Talent["skill"] < conf.TalentPriority["skill"] {
		rating.Comments += fmt.Sprintf("Elemental skill is below level %d\n", conf.TalentPriority["skill"])
	}
	if char.Talent["burst"] < conf.TalentPriority["burst"] {
		rating.Comments += fmt.Sprintf("Elemental burst is below level %d\n", conf.TalentPriority["burst"])
	}

	scoreWeapon(&intm, &rating, conf, unusedWeap)

	artifactFix(&intm, &rating, &conf)

	scoreArtifacts(&intm, &rating, conf)
	scoreRV(&intm, &rating, conf)

	rating.TotalRating = (exponentWeight + rating.RVScore) * (exponentWeight + rating.ArtifactScore) *
		(exponentWeight + rating.LevelScore) * (exponentWeight + rating.TalentScore) *
		(exponentWeight + rating.WeaponScore)
	if rating.ConstellationMet {
		rating.TotalRating *= maxCategoryMult
	} else {
		rating.TotalRating *= exponentWeight
	}
	rating.TotalRating /= float32(math.Pow(maxCategoryMult, numScoreCategories))

	rating.ArtifactScore = format.Round(rating.ArtifactScore)
	rating.ArtifactRV = format.Round(rating.ArtifactRV)
	rating.RVScore = format.Round(rating.RVScore)
	rating.ArtifactCV = format.Round(rating.ArtifactCV)
	rating.TotalRating = format.Round(rating.TotalRating)
	rating.WeaponScore = format.Round(rating.WeaponScore)
	for k, v := range rating.SubstatRVs {
		rating.SubstatRVs[k] = format.Round(v)
	}

	return rating
}

func scoreWeapon(
	intm *models.CharacterIntermediate, rating *models.CharacterRating,
	conf models.CharacterConfig, unusedWeap map[string]int,
) {
	weap := intm.Weapon
	if weap.Key == "" {
		rating.Comments += "No weapon equipped\n"
		return
	}
	if len(conf.RecommendedWeapons) == 0 {
		conf.RecommendedWeapons = []string{weap.Key}
	}
	found := false
	for _, key := range conf.RecommendedWeapons {
		if key == weap.Key {
			rating.WeaponScore = correctWeaponWeight
			found = true
			break
		}
	}
	if !found {
		ownedBetter := []string{}
		unownedBetter := []string{}
		for _, key := range conf.RecommendedWeapons {
			if _, ok := unusedWeap[key]; ok {
				ownedBetter = append(ownedBetter, key)
			} else {
				unownedBetter = append(unownedBetter, key)
			}
		}
		if len(ownedBetter) > 0 {
			rating.Comments += fmt.Sprintf(
				"Weapon is not recommended. Weapons you already own but are unused are: %s. Other weapons are: %s\n",
				format.PrintWeapons(ownedBetter),
				format.PrintWeapons(unownedBetter),
			)
		} else {
			rating.Comments += fmt.Sprintf("Weapon is not recommended. Recommended options are: %s\n",
				format.PrintWeapons(conf.RecommendedWeapons))
		}
	}
	scale := "4-3"
	if w, ok := data.Weapons[weap.Key]; ok {
		switch w.StatScale {
		case "5-0":
			scale = "5-2"
		case "4-0":
			scale = "4-2"
		case "3-0":
			scale = "3-2"
		default:
			if _, ko := data.WeaponStatScales[w.StatScale]; ko {
				scale = w.StatScale
			}
		}
		// Comment about refinement, but no score given.
		if weap.Refinement < w.Refinement {
			if strings.Contains(w.StatScale, "3-") {
				rating.Comments += fmt.Sprintf("Equipped 3 star weapon should be refinement %d. Most 3 star weapons can be obtained from wishing on any banner or found in chests in mondstadt and liyue\n", w.Refinement) //nolint: lll
			} else {
				rating.Comments += fmt.Sprintf("%s performs best when at least refinement %d\n", w.PrettyName, w.Refinement)
			}
		}
	} else {
		log.Warnf("Weapon \"%s\" not found in weapon database for character %s", weap.Key, intm.Character.Key)
	}
	atk := float32(data.WeaponStatScales[scale][weap.Level]*data.WeaponStatBase[scale] +
		data.WeaponAscensionStats[scale][weap.Ascension])
	intm.WeaponAtk = float32(data.WeaponStatScales[scale][weaponMaxLevel]*data.WeaponStatBase[scale] +
		data.WeaponAscensionStats[scale][weaponMaxAscension])

	if characterWantsSubstat(data.StatAtkPct, conf) {
		rating.WeaponScore += atk / intm.WeaponAtk * weaponLevelWeight
	} else {
		rating.WeaponScore += float32(data.WeaponSubstatScaleMult[weap.Level/5]/data.WeaponSubstatMaxMult) * weaponLevelWeight
	}
	if weap.Level < weaponMaxLevel {
		rating.Comments += "Weapon is not level 90\n"
	}
}

func scoreArtifacts(intm *models.CharacterIntermediate, rating *models.CharacterRating, conf models.CharacterConfig) {
	// Stabilize the order artifacts are iterated by using a list, and prevent badly keyed pieces from being scored.
	artifactOrder := []string{"plume", "flower", "sands", "goblet", "circlet"}
	for _, k := range artifactOrder {
		if len(intm.Artifacts) == 0 {
			break
		}
		artifact := intm.Artifacts[k]
		if artifact.SlotKey == "" {
			rating.Comments += fmt.Sprintf("No %s is equipped\n", format.PrintSlot(k))
			continue
		}
		intm.Sets[artifact.SetKey]++

		rating.ArtifactScore += float32(artifact.Level) * artifactLevelWeight
		if artifact.Rarity != artifactMaxRarity {
			rating.Comments += format.PrintSlot(k) + " is not a 5 star artifact\n"
		} else if artifact.Level < artifactMaxLevel {
			rating.Comments += format.PrintSlot(k) + " is not level 20\n"
		}
		// Calculate mainstat score.
		if k == "sands" || k == "goblet" || k == "circlet" {
			found := false
			stats := conf.AllowedMainstats[k]
			for _, key := range stats {
				if key == artifact.MainStatKey {
					rating.ArtifactScore += artifactMainStatWeight
					found = true
					break
				}
			}
			if !found {
				rating.Comments += fmt.Sprintf(
					"%s does not have an optimal main stat. Optimal main stat(s) are: %s\n",
					format.PrintSlot(artifact.SlotKey), format.PrintStats(stats),
				)
			}
		}
	}

	hasCorrectSet := false
	hasIncorrectSet := false
	twoPiece := 0
	comboAllowed := false
	for _, allowed := range conf.AllowedSets {
		if intm.Sets[allowed] >= 4 { //nolint: mnd
			rating.ArtifactScore += artifact4SetWeight
			hasCorrectSet = true
		}
		if allowed == "2pcCombo" {
			comboAllowed = true
		}
	}
	if !hasCorrectSet {
		for _, eff := range conf.AllowedEffects {
			for set, cnt := range intm.Sets {
				if cnt >= 4 { //nolint: mnd
					hasIncorrectSet = true
				}
				if data.Sets[set].SetEff == eff && cnt >= 2 && cnt < 4 {
					rating.ArtifactScore += artifact2SetWeight
					twoPiece++
				}
			}
		}
	}

	switch {
	case comboAllowed && twoPiece >= 2:
		rating.ArtifactScore += artifact2SetWeight // Add extra 2 set so it is the same as a 4 set
		rating.Comments += "Artifacts have 2 useful two-piece set bonuses, but that is recommended on this character\n"
	case hasIncorrectSet:
		rating.Comments += fmt.Sprintf("Artifacts have a non-optimal set bonus. Consider switching to a four-piece set instead. Recommended four-piece sets are: %s\n", format.PrintSets(conf.AllowedSets)) //nolint: lll
	case !hasCorrectSet && twoPiece == 0 && len(intm.Sets) > 0:
		rating.Comments += fmt.Sprintf("Artifacts have no important set effects. Recommended two-piece bonuses are: %s. Recommended four-piece sets are: %s\n", format.PrintStats(conf.AllowedEffects), format.PrintSets(conf.AllowedSets)) //nolint: lll
	case !hasCorrectSet && twoPiece == 1:
		rating.Comments += fmt.Sprintf("Artifacts have 1 useful two-piece set bonus. Consider switching to a four-piece set instead. Recommended two-piece bonuses are: %s. Recommended four-piece sets are: %s\n", format.PrintStats(conf.AllowedEffects), format.PrintSets(conf.AllowedSets)) //nolint: lll
	case !hasCorrectSet && twoPiece == 2:
		rating.Comments += fmt.Sprintf("Artifacts have 2 useful two-piece set bonuses. Consider switching to a four-piece set instead. Recommended four-piece sets are: %s\n", format.PrintSets(conf.AllowedSets)) //nolint: lll
	case len(intm.Sets) == 0:
		rating.Comments += "Character has no artifacts equipped. They will perform poorly without them\n"
	}
}

func scoreRV(intm *models.CharacterIntermediate, rating *models.CharacterRating, conf models.CharacterConfig) {
	// Stabilize the order artifacts are iterated by using a list, and prevent badly keyed pieces from being scored.
	artifactOrder := []string{"plume", "flower", "sands", "goblet", "circlet"}

	for _, sub := range conf.AllowedSubs {
		rating.SubstatRVs[sub] = 0
	}

	if len(intm.Artifacts) == 0 {
		rating.RVScore = 0
		return
	}

	for _, k := range artifactOrder {
		artifact := intm.Artifacts[k]
		if artifact.SlotKey == "" {
			continue
		}
		rv := float32(0)
		sum := float32(0)
		for _, substat := range artifact.Substats {
			for _, allowed := range conf.AllowedSubs {
				switch {
				case allowed == substat.Key:
					r := substat.Value / data.ArtifactRV[substat.Key] * pctToInt
					rv += r
					rating.SubstatRVs[allowed] += r
				case allowed == data.StatAtkPct && substat.Key == data.StatAtk &&
					!characterWantsSubstat(data.StatAtk, conf):
					r := (substat.Value / (data.MedianATK + intm.WeaponAtk) *
						pctToInt) / data.ArtifactRV[data.StatAtkPct] * pctToInt
					rv += r
					rating.SubstatRVs[allowed] += r
				case allowed == data.StatDefPct && substat.Key == data.StatDef &&
					!characterWantsSubstat(data.StatDef, conf):
					r := (substat.Value / data.MedianDEF * pctToInt) / data.ArtifactRV[data.StatDefPct] * pctToInt
					rv += r
					rating.SubstatRVs[allowed] += r
				case allowed == data.StatHpPct && substat.Key == data.StatHp &&
					!characterWantsSubstat(data.StatHp, conf):
					r := (substat.Value / data.MedianHP * pctToInt) / data.ArtifactRV[data.StatHpPct] * pctToInt
					rv += r
					rating.SubstatRVs[allowed] += r
				default:
				}
			}
			sum += substat.Value / data.ArtifactRV[substat.Key] * pctToInt
			if substat.Key == data.StatCritRate {
				intm.CritRateSum += substat.Value
			} else if substat.Key == data.StatCritDmg {
				intm.CritDamageSum += substat.Value
			}
		}
		if sum < lowRVThreshold && artifact.Rarity == artifactMaxRarity && artifact.Level == artifactMaxLevel {
			rating.Comments += format.PrintSlot(artifact.SlotKey) +
				" has a very low total RV. Consider replacing it\n"
		}
		if rv/(data.RVPerSubstat[len(conf.AllowedSubs)]) < lowRVPercent &&
			artifact.Rarity == artifactMaxRarity && artifact.Level == artifactMaxLevel {
			rating.Comments += format.PrintSlot(artifact.SlotKey) +
				" contributes to less than 10% of recommended RV. Consider replacing it\n"
		}
		rating.ArtifactRV += rv
	}
	rating.ArtifactCV = intm.CritRateSum*2.0 + intm.CritDamageSum //nolint: mnd
	if rating.ArtifactCV < conf.RecommendedCV && rating.ArtifactCV > 0 {
		rating.Comments += "Substat CV is low for this character. Consider looking for more CRIT substats\n"
	} else if intm.Weapon.Location == "SangonomiyaKokomi" && rating.ArtifactCV > 50 {
		rating.Comments += "Kokomi has -100% crit rate. Building crit on this character is discouraged"
	}

	intm.RecRV = data.RVPerSubstat[len(conf.AllowedSubs)]
	if intm.RecRV == 0 {
		intm.RecRV = 1
	}
	if rating.ArtifactRV < intm.RecRV && rating.ArtifactRV > 0 {
		rating.Comments += fmt.Sprintf(
			"Artifact substats are below recommended RV of %.0f. Recommended substats are: %s\n",
			intm.RecRV, format.PrintStats(conf.AllowedSubs))
	}
	for k, v := range rating.SubstatRVs {
		if v < lowSubThreshold {
			rating.Comments += fmt.Sprintf(
				"Substat %s has very few rolls. Consider finding artifacts with more of this stat\n",
				format.PrintStat(k),
			)
		}
	}
	rating.RVScore = float32(math.Min(float64(rating.ArtifactRV/intm.RecRV), 1))
}

// Scores talents, taking into account relative recommended levels.
// ex if a characters recommended is 2/6/10, the burst will be worth 10/18 of the total score.
// a talent can never reach a ratio of >1, so overly levelled talents do not improve score.
func scoreTalents(char models.Character, ref map[string]int) float32 {
	d := float64(ref["auto"] + ref["skill"] + ref["burst"])
	scale := data.TalentMults[data.Characters[char.Key].TalentScales["auto"]]
	a := math.Min((scale[talentLimit(char.Talent["auto"])]/scale[ref["auto"]]), 1.0) * float64(ref["auto"]) / d
	scale = data.TalentMults[data.Characters[char.Key].TalentScales["skill"]]
	s := math.Min((scale[talentLimit(char.Talent["skill"])]/scale[ref["skill"]]), 1.0) * float64(ref["skill"]) / d
	scale = data.TalentMults[data.Characters[char.Key].TalentScales["burst"]]
	b := math.Min((scale[talentLimit(char.Talent["burst"])]/scale[ref["burst"]]), 1.0) * float64(ref["burst"]) / d
	return format.Round(float32(a + s + b))
}

func characterWantsSubstat(sub string, conf models.CharacterConfig) bool {
	for _, key := range conf.AllowedSubs {
		if key == sub {
			return true
		}
	}
	return false
}

func talentLimit(lvl int) int {
	if lvl > talentMaxLevel {
		log.Warn("talent higher than max level detected")
		return talentMaxLevel
	}
	return lvl
}

// Change the character config to allow for two-piece if either of the following are true:
// - both the ascension and weapon stat are crit rate
// - The weapon has at least 30 crit rate at level 90
// The character must also ONLY want Blizzard Strayer, Obsidian Codex or Marechaussee Hunter.
//
// Add additional weapon-required substats to characters if necessary (ex favonius).
func artifactFix(intm *models.CharacterIntermediate, rating *models.CharacterRating, conf *models.CharacterConfig) {
	for _, sub := range data.Weapons[intm.Weapon.Key].AddedSubstats {
		if sub == data.StatCritRate || sub == data.StatCritDmg && conf.RecommendedCV < 50 {
			conf.RecommendedCV = 50
		}

		hasSub := false
		for _, old := range conf.AllowedSubs {
			if old == sub {
				hasSub = true
				break
			}
		}
		if !hasSub {
			conf.AllowedSubs = append(conf.AllowedSubs, sub)
			rating.Comments += fmt.Sprintf("Adding substat %s because %s is equipped\n",
				format.PrintStat(sub), format.PrintWeapon(intm.Weapon.Key))
		}
	}

	if len(conf.AllowedSets) != 1 || !(conf.AllowedSets[0] == "BlizzardStrayer" ||
		conf.AllowedSets[0] == "MarechausseeHunter" || conf.AllowedSets[0] == "ObsidianCodex") {
		return
	}
	if data.Weapons[intm.Weapon.Key].Substat.Key == data.StatCritRate &&
		data.Characters[intm.Character.Key].AscensionStat.Key == data.StatCritRate {
		rating.Comments += "Weapon and ascension stat are both crit rate; two-piece combos are also allowed for this character\n" //nolint: lll
		conf.AllowedSets = append(conf.AllowedSets, "2pcCombo")
	} else if data.Weapons[intm.Weapon.Key].Substat.Key == data.StatCritRate &&
		data.Weapons[intm.Weapon.Key].Substat.Value*float32(data.WeaponSubstatMaxMult) > 30.0 {
		rating.Comments += "Weapon secondary stat has high crit rate; two-piece combos are also allowed for this character\n" //nolint: lll
		conf.AllowedSets = append(conf.AllowedSets, "2pcCombo")
	}
}
