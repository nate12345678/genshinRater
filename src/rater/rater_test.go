package rater

import (
	"encoding/json"
	"os"
	"testing"

	"gitlab.com/nate12345678/genshinRater/src/config"
	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/marshal"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

// Test that the output hasn't changed.
func TestRaterBasic(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	conf := models.Config{}
	if err := config.GetConfig(&conf); err != nil {
		t.Fail()
	}
	input := models.IGOOD{}
	if err := marshal.FromFile("test-files/input-test.json", &input); err != nil {
		t.Fail()
	}
	characterRating, err := Run(input, conf)
	if err != nil {
		t.Fail()
	}

	outJSON, err := json.MarshalIndent(characterRating, "", "  ")
	if err != nil {
		t.Error("failed to marshal ratings")
	}

	gold, err := os.ReadFile("test-files/output-gold.json")
	if err != nil {
		t.Error("failed to read test output file")
	}

	if err != nil || string(gold) != string(outJSON) {
		marshal.ToFile("output-test.json", characterRating) //nolint: errcheck
		t.Error("Output does not match gold, saving output for manual inspection.")
	}
}

func TestBuildIntermediates(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	conf := models.Config{}
	if err := config.GetConfig(&conf); err != nil {
		t.Fail()
	}
	input := models.IGOOD{}
	if err := marshal.FromFile("test-files/input-test.json", &input); err != nil {
		t.Fail()
	}
	intm := map[string]models.CharacterIntermediate{}
	buildIntermediates(input, intm, conf)
	if len(intm) != len(input.Characters) {
		t.Errorf("Expected %d intermediates, got %d", len(input.Characters), len(intm))
	}
	ayaya := intm["KamisatoAyaka"]
	if ayaya.Character.Key != "KamisatoAyaka" {
		t.Errorf("Expected KamisatoAyaka, got %s", ayaya.Character.Key)
	}
	if len(ayaya.Artifacts) != 5 {
		t.Errorf("Expected 5 artifacts, got %d", len(ayaya.Artifacts))
	}
	if ayaya.Weapon.Key != "PrimordialJadeCutter" {
		t.Errorf("Expected PrimordialJadeCutter, got %s", ayaya.Weapon.Key)
	}
}

func TestFixTraveler(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	conf := models.Config{}
	if err := config.GetConfig(&conf); err != nil {
		t.Fail()
	}
	input := models.IGOOD{}
	if err := marshal.FromFile("test-files/traveler-test.json", &input); err != nil {
		t.Fail()
	}

	fixTraveler(&input)
	for _, artifact := range input.Artifacts {
		if artifact.Location == "traveler" {
			t.Error("Artifact equipped to traveler")
		}
	}
	for _, weap := range input.Weapons {
		if weap.Location == "traveler" {
			t.Error("Weapon equipped to traveler")
		}
	}

	intm := map[string]models.CharacterIntermediate{}

	buildIntermediates(input, intm, conf)
	if len(intm["TravelerAnemo"].Artifacts) != 5 {
		t.Errorf("Expected 5 artifacts for Traveler Anemo, got %d", len(intm["TravelerAnemo"].Artifacts))
	}
	if len(intm["TravelerDendro"].Artifacts) != 5 {
		t.Errorf("Expected 5 artifacts for Traveler Dendro, got %d", len(intm["TravelerDendro"].Artifacts))
	}
	if len(intm["TravelerPyro"].Artifacts) != 5 {
		t.Errorf("Expected 5 artifacts for Traveler Pyro, got %d", len(intm["TravelerPyro"].Artifacts))
	}
}

func TestRateAccount(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	conf := models.Config{}
	if err := config.GetConfig(&conf); err != nil {
		t.Fail()
	}
	input := models.IGOOD{}
	if err := marshal.FromFile("test-files/input-test.json", &input); err != nil {
		t.Fail()
	}
	characterRating, err := Run(input, conf)
	if err != nil {
		t.Fail()
	}

	acct := RateAccount(input.Characters, characterRating)
	if acct.CharsPerEle["cryo"] != 2 {
		t.Errorf("Expected 2 Cryo chars, got %d", acct.CharsPerEle["cryo"])
	}
	if acct.CharsPerRole["on"] != 2 {
		t.Errorf("Expected 2 On chars, got %d", acct.CharsPerRole["on"])
	}
	if acct.CharsPerRating["A"] != 3 {
		t.Errorf("Expected 3 A rated chars, got %d", acct.CharsPerRating["A"])
	}
}
