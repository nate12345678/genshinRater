package rater

import (
	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/format"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

const (
	minTheaterLevel = 70
)

var (
	elemsMain = []string{"pyro", "hydro", "electro", "cryo"}
	elemsSide = []string{"pyro", "hydro", "electro", "cryo", "anemo", "dendro", "geo"}
)

func RateAccount(input []models.Character, ratings map[string]models.CharacterRating) models.AccountRating {
	acct := models.AccountRating{
		CharsPerEle:     map[string]int{"anemo": 0, "cryo": 0, "dendro": 0, "electro": 0, "geo": 0, "hydro": 0, "pyro": 0},
		CharsPerRole:    map[string]int{"on": 0, "off": 0, "DPS": 0, "Support": 0, "Survivability": 0},
		CharsPerRating:  map[string]int{"S": 0, "A": 0, "B": 0, "C": 0, "D": 0, "F": 0},
		CharsPerTheater: map[string]int{},
	}

	for _, char := range input {
		name := char.Key
		if char.Level >= minTheaterLevel {
			acct.CharsPerEle[data.Characters[name].Element]++
			if data.Characters[name].Onfield {
				acct.CharsPerRole["on"]++
			} else {
				acct.CharsPerRole["off"]++
			}
			for _, role := range data.Characters[name].Roles {
				acct.CharsPerRole[role]++
			}
		}
		acct.CharsPerRating[format.ScoreToRating(ratings[name].TotalRating)]++
	}

	for i := 0; i < len(elemsMain)-1; i++ {
		ele1 := elemsMain[i]
		for j := i + 1; j < len(elemsMain); j++ {
			ele2 := elemsMain[j]
			for _, ele3 := range elemsSide {
				acct.CharsPerTheater[ele1+ele2+ele3] = acct.CharsPerEle[ele1] + acct.CharsPerEle[ele2] + acct.CharsPerEle[ele3]
			}
		}
	}
	return acct
}
