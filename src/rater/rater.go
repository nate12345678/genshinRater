package rater

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/nate12345678/genshinRater/src/config"
	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/log"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

func Run(input models.IGOOD, conf models.Config) (map[string]models.CharacterRating, error) {
	if input.Format != models.AllowedFormat {
		return map[string]models.CharacterRating{}, errors.New("input data does not match GOOD version")
	}
	if conf.Version != models.ConfigVersion {
		return map[string]models.CharacterRating{},
			errors.New("config format version does not match tool format version")
	}

	// characterConfigs = conf.CharacterConfigs
	characterArtifacts := map[string]models.CharacterIntermediate{}
	characterRating := map[string]models.CharacterRating{}

	fixTraveler(&input)

	// Attach gear to character intermediates.
	buildIntermediates(input, characterArtifacts, conf)

	// Configure Character builds based on equipped weapons
	// Comments need to be prepended later since we haven't yet created the rating
	buildComments := configureBuilds(input, conf)

	unused := map[string]int{}
	for _, weap := range input.Weapons {
		if weap.Location == "" {
			unused[weap.Key]++
		}
	}

	// Score each character individually
	for k, v := range characterArtifacts {
		characterRating[k] = scoreCharacter(conf.CharacterConfigs[k], v, unused)
	}

	// Prepend any build comments
	for name, rating := range characterRating {
		if v, ok := buildComments[name]; ok {
			rating.Comments = v + characterRating[name].Comments
			characterRating[name] = rating
		}
	}

	return characterRating, nil
}

func configureBuilds(input models.IGOOD, conf models.Config) map[string]string {
	comments := map[string]string{}
	for _, weap := range input.Weapons {
		if weap.Location == "" {
			continue
		}
		if build, ok := conf.BuildModifiers[weap.Location][weap.Key]; ok {
			log.Infof("found custom build %s for character %s with weapon %s", build, weap.Location, weap.Key)
			mod := conf.CustomBuilds[build]
			cc := conf.CharacterConfigs[weap.Location]
			cc = config.ModifyBuild(cc, mod)
			// Add the weapon since it might not be in the recommended weapons list but should be scored as correct.
			cc.RecommendedWeapons = append(cc.RecommendedWeapons, weap.Key)
			conf.CharacterConfigs[weap.Location] = cc
			comments[weap.Location] = fmt.Sprintf(
				"Using a %s build because of equipped weapon %s. To rate against the default build, change weapons\n",
				mod.BuildHelp, data.Weapons[weap.Key].PrettyName)
		}
	}
	return comments
}

// Moves artifacts from the baseline traveler to the selected one.
func fixTraveler(input *models.IGOOD) {
	variants := make([]string, 0, len(data.TravelerElements))
	for _, c := range input.Characters {
		if strings.Contains(c.Key, "Traveler") {
			variants = append(variants, c.Key)
		}
	}
	for i, art := range input.Artifacts {
		if art.Location != "Traveler" {
			continue
		}
		for j, v := range variants {
			newArt := art
			newArt.Location = v
			if j == 0 {
				input.Artifacts[i] = newArt
				continue
			}
			input.Artifacts = append(input.Artifacts, newArt)
		}
	}
	for i, weap := range input.Weapons {
		if weap.Location != "Traveler" {
			continue
		}
		for j, v := range variants {
			newWeap := weap
			newWeap.Location = v
			if j == 0 {
				input.Weapons[i] = newWeap
				continue
			}
			input.Weapons = append(input.Weapons, newWeap)
		}
	}
}

func buildIntermediates(input models.IGOOD, intermediates map[string]models.CharacterIntermediate, conf models.Config) {
	var currentChar models.CharacterIntermediate
	for _, char := range input.Characters {
		if _, ok := intermediates[char.Key]; !ok {
			currentChar = models.CharacterIntermediate{
				Sets:          map[string]int{},
				Artifacts:     map[string]models.Artifact{},
				Weapon:        models.Weapon{},
				Character:     models.Character{},
				CritRateSum:   0,
				CritDamageSum: 0,
			}
		} else {
			currentChar = intermediates[char.Key]
		}
		if char.Level > characterMaxLevel {
			char.Level = characterMaxLevel
			log.Warnf("%s has too high level. Clamping to 90", char.Key)
		}
		if char.Ascension > charMaxAscension {
			char.Ascension = charMaxAscension
			log.Warnf("%s has too high ascension. Clamping to 6", char.Key)
		}
		currentChar.Character = char
		intermediates[char.Key] = currentChar
		if _, ok := conf.CharacterConfigs[char.Key]; !ok {
			log.Warnf("no config for character %s, using default", char.Key)
			conf.CharacterConfigs[char.Key] = models.DefaultConfig()
		}
	}

	for _, weap := range input.Weapons {
		if weap.Location == "" {
			continue
		}
		if _, ok := intermediates[weap.Location]; !ok {
			log.Warnf("Weapon %s, id %s found on missing character %s, skipping", weap.Key, weap.GetID(), weap.Location)
			continue
		}
		currentChar = intermediates[weap.Location]
		if currentChar.Weapon.Key != "" {
			log.Warnf("Multiple weapons detected for character %s, skipping weapon %s, id %s",
				weap.Location, weap.Key, weap.GetID())
			continue
		}
		if weap.Level > weaponMaxLevel {
			weap.Level = weaponMaxLevel
			log.Warnf("Weapon %s, id %s has too high level. Clamping to 90", weap.Location, weap.Key, weap.GetID())
		}
		if weap.Ascension > weaponMaxAscension {
			weap.Level = weaponMaxAscension
			log.Warnf("Weapon %s, id %s has too high ascension. Clamping to 6", weap.Location, weap.Key, weap.GetID())
		}
		currentChar.Weapon = weap
		intermediates[weap.Location] = currentChar
	}

	for _, art := range input.Artifacts {
		if art.Location == "" {
			continue
		}
		if _, ok := intermediates[art.Location]; !ok {
			log.Warnf("artifact found for missing character %s, skipping artifact %s", art.Location, art.GetID())
			continue
		}
		currentChar = intermediates[art.Location]

		if art.Level > artifactMaxLevel {
			art.Level = artifactMaxLevel
			log.Warnf("Artifact %s has too high level. Clamping to 20", art.GetID())
		}
		if _, ok := currentChar.Artifacts[art.SlotKey]; ok {
			log.Warnf("multiple artifacts for slot %s detected for character %s, skipping artifact %s",
				art.SlotKey, art.Location, art.GetID())
			continue
		}
		currentChar.Artifacts[art.SlotKey] = art
		intermediates[art.Location] = currentChar
	}
}
