package format

import (
	"fmt"
	"math"
	"strings"

	"gitlab.com/nate12345678/genshinRater/src/data"
)

// Round a float32 to 3 decimal places.
func Round(n float32) float32 {
	return float32(math.Round(float64(n)*1000.0) / 1000.0) //nolint: mnd
}

func ScoreToRating(f float32) string {
	switch {
	case f >= 1:
		return "S"
	case f >= 0.88: //nolint: mnd
		return "A"
	case f >= 0.76: //nolint: mnd
		return "B"
	case f >= 0.64: //nolint: mnd
		return "C"
	case f >= 0.52: //nolint: mnd
		return "D"
	default:
		return "F"
	}
}

func SetKeysToNames(keys []string) []string {
	ret := make([]string, len(keys))
	for i, k := range keys {
		if set, ok := data.Sets[k]; ok {
			ret[i] = set.PrettyName
		} else {
			ret[i] = k
		}
	}
	return ret
}

func PrintSets(keys []string) string {
	s := ""
	if len(keys) == 0 {
		return s
	}
	for _, k := range keys {
		if set, ok := data.Sets[k]; ok {
			s += set.PrettyName + ", "
		} else {
			s += k + ", "
		}
	}
	return s[:len(s)-2]
}

func StatsToPretty(stats []string) []string {
	return strings.Split(PrintStats(stats), ", ")
}

func PrintStat(stat string) string {
	s := strings.ReplaceAll(stat, "atk", "ATK")
	s = strings.ReplaceAll(s, "def", "DEF")
	s = strings.ReplaceAll(s, "hp", "HP")
	s = strings.ReplaceAll(s, "eleMas", "Elemental Mastery")
	s = strings.ReplaceAll(s, "critRate", "CRIT Rate")
	s = strings.ReplaceAll(s, "critDMG", "CRIT DMG")
	s = strings.ReplaceAll(s, "enerRech", "Energy Recharge")
	s = strings.ReplaceAll(s, "anemo_dmg", "Anemo Damage")
	s = strings.ReplaceAll(s, "cryo_dmg", "Cryo Damage")
	s = strings.ReplaceAll(s, "dendro_dmg", "Dendro Damage")
	s = strings.ReplaceAll(s, "electro_dmg", "Electro Damage")
	s = strings.ReplaceAll(s, "geo_dmg", "Geo Damage")
	s = strings.ReplaceAll(s, "hydro_dmg", "Hydro Damage")
	s = strings.ReplaceAll(s, "pyro_dmg", "Pyro Damage")
	s = strings.ReplaceAll(s, "physical_dmg", "Physical Damage")
	s = strings.ReplaceAll(s, "heal", "Healing Bonus")
	s = strings.ReplaceAll(s, "auto", "Normal Attack Damage%")
	s = strings.ReplaceAll(s, "skill", "Skill Damage%")
	s = strings.ReplaceAll(s, "burst", "Burst Damage%")
	s = strings.ReplaceAll(s, "plunge", "Plunge Attack Damage%")
	s = strings.ReplaceAll(s, "nightsoul_dmg", "Nightsoul State Damage")
	s = strings.ReplaceAll(s, "nightsoulEner", "Nightsoul Energy Gain")
	s = strings.ReplaceAll(s, "_", "%")
	return s
}

func PrintStats(stats []string) string {
	if len(stats) == 0 {
		return ""
	}
	s := stats
	for i, v := range stats {
		s[i] = PrintStat(v)
	}
	return strings.Join(s, ", ")
}

func PrintSlot(key string) string {
	switch strings.ToLower(key) {
	case "circlet":
		return "Circlet"
	case "flower":
		return "Flower"
	case "goblet":
		return "Goblet"
	case "plume":
		return "Plume"
	case "sands":
		return "Sands"
	}
	return "INVALID KEY"
}

func PrintWeapon(weap string) string {
	return PrintWeapons([]string{weap})
}

func PrintWeapons(weaps []string) string {
	if len(weaps) == 0 {
		return ""
	}
	s := fmt.Sprintf("%v", weaps)
	s = strings.ReplaceAll(s, " ", ", ")
	for _, v := range weaps {
		if n, ok := data.Weapons[v]; ok {
			s = strings.Replace(s, v, n.PrettyName, 1)
		}
	}
	return s[1 : len(s)-1]
}

func WeaponKeysToNames(weaps []string) []string {
	ret := make([]string, len(weaps))
	for i, v := range weaps {
		if _, ok := data.Weapons[v]; ok {
			ret[i] = data.Weapons[v].PrettyName
		} else {
			ret[i] = v
		}
	}
	return ret
}
