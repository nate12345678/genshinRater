package format

import (
	"testing"

	"gitlab.com/nate12345678/genshinRater/src/data"
)

func TestPrinStat(t *testing.T) {
	t.Parallel()
	t1 := "cryo_dmg_"
	t2 := "fakePercent_"
	if PrintStat(t1) != "Cryo Damage%" {
		t.Fail()
	}
	if PrintStat(t2) != "fakePercent%" {
		t.Fail()
	}
}

func TestPrintWeap(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	w0 := "DullBlade"
	w1 := "SongOfBrokenPines"
	w2 := "fakeweapon"
	r1 := PrintWeapons([]string{w0, w1, w2})
	if r1 != "Dull Blade, Song Of Broken Pines, fakeweapon" {
		t.Fail()
	}
	r2 := WeaponKeysToNames([]string{w0, w1, w2})
	if r2[0] != "Dull Blade" || r2[1] != "Song Of Broken Pines" || r2[2] != "fakeweapon" {
		t.Fail()
	}
}

func TestFormatSet(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	s0 := "NoblesseOblige"
	s1 := "GildedDreams"
	s2 := "fakeset"
	r1 := PrintSets([]string{s0, s1, s2})
	if r1 != "Noblesse Oblige, Gilded Dreams, fakeset" {
		t.Fail()
	}
	r2 := SetKeysToNames([]string{s0, s1, s2})
	if r2[0] != "Noblesse Oblige" || r2[1] != "Gilded Dreams" || r2[2] != "fakeset" {
		t.Fail()
	}
}
