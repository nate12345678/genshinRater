// Package log implments a simple 3-level logging function as a wrapper around the default logger from pkg.go.dev/log.
package log

import "log"

func Print(msg ...any) {
	log.Print(msg...)
}

func Printf(msg string, v ...any) {
	log.Printf(msg, v...)
}

func Info(msg string) {
	Infof(msg)
}

func Infof(msg string, v ...any) {
	log.Printf("info: "+msg, v...)
}

func Warn(msg string) {
	Warnf(msg)
}

func Warnf(msg string, v ...any) {
	log.Printf("warn: "+msg, v...)
}

func Error(msg string) {
	Errorf(msg)
}

func Errorf(msg string, v ...any) {
	log.Printf("error: "+msg, v...)
}
