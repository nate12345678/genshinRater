package config

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	genshinrater "gitlab.com/nate12345678/genshinRater"
	"gitlab.com/nate12345678/genshinRater/src/log"
	"gitlab.com/nate12345678/genshinRater/src/marshal"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

const (
	URI = "https://gitlab.com/nate12345678/genshinRater/-/raw/main/config.json"
)

func FetchConfig() error {
	c := new(http.Client)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second) //nolint: mnd
	defer cancel()
	var b io.Reader
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, URI, b)
	if err != nil {
		return err
	}
	resp, err := c.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("non-success status code returned: %d", resp.StatusCode)
	}
	body := []byte{}
	if resp.ContentLength > 0 {
		body, err = io.ReadAll(resp.Body)
		if err != nil {
			return err
		}
	}

	var conf models.Config
	if err = json.Unmarshal(body, &conf); err != nil {
		return err
	}

	if conf.Version != models.ConfigVersion {
		return errors.New("downloaded config is a different version from the tool, keeping existing config")
	}

	return marshal.ToFile("config.json", conf)
}

func GetConfig(c *models.Config) error {
	if err := marshal.FromFile("config.json", c); err != nil {
		log.Warnf("failed to read default config: %s", err)
		log.Info("getting default config")
		return DefaultConfig(c)
	}
	return nil
}

func DefaultConfig(c *models.Config) error {
	if err := json.Unmarshal(genshinrater.DefaultConfig, &c); err != nil {
		return fmt.Errorf("failed to read default config: %w", err)
	}
	return nil
}

func SaveConfig(c models.Config) error {
	return marshal.ToFile("config.json", c)
}

func ModifyBuild(conf models.CharacterConfig, mod models.CustomBuild) models.CharacterConfig {
	cc := conf
	cc.AllowedMainstats = mod.AllowedMainstats
	cc.AllowedSets = mod.AllowedSets
	cc.AllowedSubs = mod.AllowedSubs
	cc.AllowedEffects = mod.AllowedEffects
	cc.TalentPriority = mod.TalentPriority
	cc.RecommendedConstellation = mod.RecommendedConstellation
	cc.RecommendedCV = mod.RecommendedCV
	return cc
}
