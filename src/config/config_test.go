package config

import (
	"testing"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

func TestGetConfig(t *testing.T) {
	t.Parallel()
	var c1 models.Config
	var c2 models.Config
	if err := GetConfig(&c1); err != nil {
		t.Fail()
	}
	if err := DefaultConfig(&c2); err != nil {
		t.Fail()
	}
	if c1.Version != c2.Version ||
		c1.CharacterConfigs["Albedo"].AllowedSubs[0] != c2.CharacterConfigs["Albedo"].AllowedSubs[0] {
		t.Fail()
	}
}

func TestPrettyConfig(t *testing.T) { //nolint: paralleltest
	// t.Parallel()
	data.LoadData()
	data.ReadCompatMatrix()
	var c models.Config
	if err := DefaultConfig(&c); err != nil {
		t.Fail()
	}
	p := toPrettyConf("HuTao", c.CharacterConfigs["HuTao"])
	if p.Nickname != "Hu Tao" || p.Builds[0].NormalAtkLvl != 10 {
		t.Fail()
	}
}
