package config

import (
	"bytes"
	_ "embed"
	"html/template"
	"slices"
	"strings"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/format"
	"gitlab.com/nate12345678/genshinRater/src/models"
	"gitlab.com/nate12345678/genshinRater/src/teamcomp"
)

//go:embed template.html
var htmlTmpl string

const (
	jumpTmpl     = `Jump to:<br>&bull;{{range .Jumps}}&thinsp;<a href="#{{.Name}}">{{.Alpha}}</a>&thinsp;&bull;{{end}}`
	numTeammates = 7
)

type JumpItem struct {
	Alpha string
	Name  string
}

type PrettyBuild struct {
	Key                      string
	Description              string
	AllowedSets              []string
	AllowedEffects           []string
	AllowedSubs              []string
	CircletStat              string
	GobletStat               string
	SandsStat                string
	RecommendedWeapons       []string
	NormalAtkLvl             int
	SkillLvl                 int
	BurstLvl                 int
	RecommendedConstellation int
	RecommendedCV            int
}

// Config suitable for templating.
type PrettyConfig struct {
	Nickname    string
	Location    string
	Element     string
	Description string
	Rarity      int
	WeaponType  string
	Roles       []string
	Teammates   []string
	Builds      []PrettyBuild
}

// Reads in the config, and then creates an HTML of all of the character builds.
func MakePrettyConfig() ([]byte, error) {
	type ConfItem struct {
		Key   string
		Value PrettyConfig
	}
	type Out struct {
		Jumps []JumpItem
		Chars []ConfItem
	}
	conf := models.Config{}
	prettyConfs := map[string]PrettyConfig{}
	if err := GetConfig(&conf); err != nil {
		return []byte{}, err
	}

	for k, v := range conf.CharacterConfigs {
		prettyConfs[k] = toPrettyConf(k, v)
	}

	for name, builds := range conf.BuildModifiers {
		bNames := map[string][]string{}
		for w, b := range builds {
			bNames[b] = append(bNames[b], w)
		}
		for bName, weaps := range bNames {
			custom := ModifyBuild(conf.CharacterConfigs[name], conf.CustomBuilds[bName])
			custom.RecommendedWeapons = weaps
			pc := prettyConfs[name]
			pc.Builds = append(pc.Builds,
				toPrettyBuild(custom, bName, conf.CustomBuilds[bName].BuildHelp),
			)
			prettyConfs[name] = pc
		}
	}

	names := make([]string, len(conf.CharacterConfigs))
	i := 0
	for k := range conf.CharacterConfigs {
		names[i] = k
		i++
	}
	slices.Sort(names)

	out := Out{
		nameJumpList(names),
		make([]ConfItem, 0, len(conf.CharacterConfigs)),
	}
	for name, rating := range prettyConfs {
		out.Chars = append(out.Chars, ConfItem{
			Key:   name,
			Value: rating,
		})
	}

	slices.SortStableFunc(out.Chars, func(a, b ConfItem) int {
		return strings.Compare(a.Key, b.Key)
	})

	tmpl, err := template.New("output").Parse(htmlTmpl)
	if err != nil {
		return nil, err
	}

	var tpl bytes.Buffer
	if err = tmpl.Execute(&tpl, out); err != nil {
		return []byte{}, err
	}
	return tpl.Bytes(), nil
}

func toPrettyConf(name string, c models.CharacterConfig) PrettyConfig {
	p := PrettyConfig{}
	p.Nickname = data.Characters[name].PrettyName
	p.Element = data.Characters[name].Element
	p.Roles = data.Characters[name].Roles
	p.WeaponType = data.Characters[name].WeaponType
	p.Location = "Off-Field"
	if data.Characters[name].Onfield {
		p.Location = "On-Field"
	}
	p.Description = data.Characters[name].Description
	p.Rarity = data.Characters[name].Rarity

	p.Builds = []PrettyBuild{toPrettyBuild(c, "Default", "Default")}
	p.Teammates = teamcomp.GetPriorityTeammates(name, numTeammates)
	for i, v := range p.Teammates {
		p.Teammates[i] = data.Characters[v].PrettyName
	}

	return p
}

func toPrettyBuild(c models.CharacterConfig, key, description string) PrettyBuild {
	return PrettyBuild{
		AllowedSubs:              format.StatsToPretty(c.AllowedSubs),
		AllowedSets:              format.SetKeysToNames(c.AllowedSets),
		AllowedEffects:           format.StatsToPretty(c.AllowedEffects),
		RecommendedWeapons:       format.WeaponKeysToNames(c.RecommendedWeapons),
		RecommendedConstellation: c.RecommendedConstellation,
		RecommendedCV:            int(c.RecommendedCV),
		CircletStat:              format.PrintStats(c.AllowedMainstats["circlet"]),
		GobletStat:               format.PrintStats(c.AllowedMainstats["goblet"]),
		SandsStat:                format.PrintStats(c.AllowedMainstats["sands"]),
		NormalAtkLvl:             c.TalentPriority["auto"],
		SkillLvl:                 c.TalentPriority["skill"],
		BurstLvl:                 c.TalentPriority["burst"],
		Key:                      key,
		Description:              description,
	}
}

func nameJumpList(names []string) []JumpItem {
	alpha := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	l := make([]JumpItem, len(alpha))

	for i := range alpha {
		l[i].Alpha = string(alpha[i])
	}

	lidx := 0
	for i := range names {
		for alpha[lidx] < names[i][0] {
			l[lidx].Name = names[i]
			lidx++
		}
		if alpha[lidx] == names[i][0] {
			l[lidx].Name = names[i]
			lidx++
		}
	}
	return l
}
