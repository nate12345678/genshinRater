package data

import "testing"

// Doesn't really test anything, but will cause tests that require data to fail if run in parallel.
func TestLoadData(t *testing.T) {
	t.Parallel()
	LoadData()
	if len(Characters) < 50 {
		t.Fail()
	}
	ReadCompatMatrix()
	if CompatMatrix[0][0] != 0 {
		t.Fail()
	}
}
