package marshal

import (
	"os"
	"testing"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

func TestMarshalHTMLPretty(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	input := map[string]models.CharacterRating{}
	if err := FromFile("test-data.json", &input); err != nil {
		t.Fail()
	}

	outHTML, err := HTMLPretty(input, OrderAlpha)
	if err != nil {
		t.Error("failed to generate output html: %w", err)
	}

	gold, err := os.ReadFile("output-gold")
	if err != nil {
		t.Error("failed to read test output file")
	}

	if err != nil || string(gold) != string(outHTML) {
		os.WriteFile("output-test.html", outHTML, os.ModePerm) //nolint: gosec,errcheck
		t.Error("html output does not match gold standard. Saving to output-test.html for inspection.")
	}
}
