package marshal

import (
	"bytes"
	_ "embed"
	"html/template"
	"sort"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

// Using embed allows for no additional file without requiring writing the html as a go string.
//
//go:embed template.html
var htmlTmpl string

//go:embed account.html
var acctTmpl string

const (
	MinTheaterShown = 3
)

func HTMLAccount(account models.AccountRating) ([]byte, error) {
	elemsMain := []string{"pyro", "hydro", "electro", "cryo"}
	elemsSide := []string{"pyro", "hydro", "electro", "cryo", "anemo", "dendro", "geo"}
	theaterTargets := map[string]int{"Hard": 13, "Visionary": 19} //nolint: mnd

	p := PrettyAccount{
		CharsPerEle:    account.CharsPerEle,
		CharsPerRole:   account.CharsPerRole,
		CharsPerRating: account.CharsPerRating,
		TooFewTheater:  [][]string{},
		Comments:       account.Comments,
	}

	for _, cnt := range p.CharsPerEle {
		p.TotalChars += cnt
	}

	// Check Theater difficulty in increasing order. If too many are incompletable, stop increasing difficulty.
	for lvl, target := range theaterTargets {
		p.TheaterLevel = lvl
		for i := 0; i < len(elemsMain)-1; i++ {
			ele1 := elemsMain[i]
			for j := i + 1; j < len(elemsMain); j++ {
				ele2 := elemsMain[j]
				for k := j + 1; k < len(elemsSide); k++ {
					ele3 := elemsSide[k]
					if account.CharsPerTheater[ele1+ele2+ele3] < target {
						p.TooFewTheater = append(p.TooFewTheater, []string{ele1, ele2, ele3})
					}
				}
			}
		}
		if len(p.TooFewTheater) > MinTheaterShown {
			break
		}
	}

	tmpl, err := template.New("output").Parse(acctTmpl)
	if err != nil {
		return []byte{}, err
	}

	var tpl bytes.Buffer
	if err = tmpl.Execute(&tpl, p); err != nil {
		return []byte{}, err
	}
	return tpl.Bytes(), nil
}

// Takes various rating data and composes a usable html output.
// The rating-specific data is held within the <section></section> html tags and can be safely extracted to use inside
// another html container.
func HTMLPretty( //nolint: funlen
	r map[string]models.CharacterRating, order string,
) ([]byte, error) {
	type OrderedMap struct {
		Key   string
		Value PrettyRating
	}
	out := make([]OrderedMap, 0, len(r))
	for name, rating := range r {
		out = append(out, OrderedMap{
			Key:   name,
			Value: makePrettyRating(rating, data.Characters[name]),
		})
	}

	var f func(i, j int) bool
	switch order {
	default:
		fallthrough
	case OrderAlpha:
		f = func(i, j int) bool {
			return out[i].Key < out[j].Key
		}
	case OrderAlphaRev:
		f = func(i, j int) bool {
			return out[i].Key > out[j].Key
		}
	case OrderRV:
		f = func(i, j int) bool {
			return r[out[i].Key].ArtifactRV > r[out[j].Key].ArtifactRV ||
				(r[out[i].Key].ArtifactRV == r[out[j].Key].ArtifactRV && out[i].Key < out[j].Key)
		}
	case OrderRVRev:
		f = func(i, j int) bool {
			return r[out[i].Key].ArtifactRV < r[out[j].Key].ArtifactRV ||
				(r[out[i].Key].ArtifactRV == r[out[j].Key].ArtifactRV && out[i].Key < out[j].Key)
		}
	case OrderScore:
		f = func(i, j int) bool {
			return r[out[i].Key].TotalRating > r[out[j].Key].TotalRating ||
				(r[out[i].Key].TotalRating == r[out[j].Key].TotalRating && out[i].Key < out[j].Key)
		}
	case OrderScoreRev:
		f = func(i, j int) bool {
			return r[out[i].Key].TotalRating < r[out[j].Key].TotalRating ||
				(r[out[i].Key].TotalRating == r[out[j].Key].TotalRating && out[i].Key < out[j].Key)
		}
	case OrderCV:
		f = func(i, j int) bool {
			return r[out[i].Key].ArtifactCV > r[out[j].Key].ArtifactCV ||
				(r[out[i].Key].ArtifactCV == r[out[j].Key].ArtifactCV && out[i].Key < out[j].Key)
		}
	case OrderCVRev:
		f = func(i, j int) bool {
			return r[out[i].Key].ArtifactCV < r[out[j].Key].ArtifactCV ||
				(r[out[i].Key].ArtifactCV == r[out[j].Key].ArtifactCV && out[i].Key < out[j].Key)
		}
	case OrderGame:
		f = func(i, j int) bool {
			if r[out[i].Key].LevelScore > r[out[j].Key].LevelScore {
				return true
			} else if r[out[i].Key].LevelScore < r[out[j].Key].LevelScore {
				return false
			}

			if data.Characters[out[i].Key].Rarity > data.Characters[out[j].Key].Rarity {
				return true
			} else if data.Characters[out[i].Key].Rarity < data.Characters[out[j].Key].Rarity {
				return false
			}

			if data.GameElemOrder[data.Characters[out[i].Key].Element] <
				data.GameElemOrder[data.Characters[out[j].Key].Element] {
				return true
			} else if data.GameElemOrder[data.Characters[out[i].Key].Element] >
				data.GameElemOrder[data.Characters[out[j].Key].Element] {
				return false
			}

			if data.Characters[out[i].Key].ReleaseOrder > data.Characters[out[j].Key].ReleaseOrder {
				return true
			}

			return false
		}
	}
	sort.SliceStable(out, f)

	tmpl, err := template.New("output").Parse(htmlTmpl)
	if err != nil {
		return []byte{}, err
	}

	var tpl bytes.Buffer
	if err = tmpl.Execute(&tpl, out); err != nil {
		return []byte{}, err
	}
	return tpl.Bytes(), nil
}
