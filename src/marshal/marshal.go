package marshal

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/format"
	"gitlab.com/nate12345678/genshinRater/src/models"
)

// Ratings suitable for templating to html.
type PrettyRating struct {
	Nickname            string
	Element             string
	ArtifactRV          string
	RVScore             int
	RVRating            string
	ArtifactCV          string
	ArtifactScore       int
	ArtifactRating      string
	LevelScore          int
	LevelRating         string
	TalentScore         int
	TalentRating        string
	WeaponScore         int
	WeaponRating        string
	ConstellationMet    string
	ConstellationRating string
	TotalScore          int
	TotalRating         string
	Rarity              int
	WeaponType          string
	Roles               []string
	Location            string
	Comments            []string
	SubRVs              map[string]string
}

type PrettyAccount struct {
	CharsPerEle    map[string]int
	CharsPerRole   map[string]int
	CharsPerRating map[string]int
	TheaterLevel   string
	TooFewTheater  [][]string
	TotalChars     int
	Comments       string
}

const (
	OrderAlpha    = "alpha"
	OrderAlphaRev = "alphaRev"
	OrderRV       = "rollValue"
	OrderRVRev    = "rollValueRev"
	OrderScore    = "score"
	OrderScoreRev = "scoreRev"
	OrderCV       = "critValue"
	OrderCVRev    = "critValueRev"
	OrderGame     = "gameDefault"

	pctToInt = 100.0
)

// loads an entire file into a byte array. This is *probably* safe for most application loads since
// we will be putting the entire contents into memory anyway.
func LoadFile(filepath string) ([]byte, error) {
	contents, err := os.ReadFile(filepath)
	if err != nil {
		return []byte{}, fmt.Errorf("failed to load file: %w", err)
	}

	return contents, nil
}

// A simple wrapper that loads the contents of a json file into v, allowing data to be stored as json
// making management of game data definitions easier. Can also be used to load user configs.
func FromFile(filepath string, v any) error {
	c, err := LoadFile(filepath)
	if err != nil {
		return fmt.Errorf("failed to unmarshal: %w", err)
	}
	err = json.Unmarshal(c, v)
	if err != nil {
		return fmt.Errorf("failed to unmarshal: %w", err)
	}
	return nil
}

func ToFile(filepath string, v any) error {
	b, err := json.MarshalIndent(v, "", "  ")
	if err != nil {
		return err
	}
	err = os.WriteFile(filepath, b, os.ModePerm) //nolint: gosec
	if err != nil {
		return err
	}
	return nil
}

func makePrettyRating(r models.CharacterRating, d data.CharacterData) PrettyRating {
	p := PrettyRating{}
	p.Nickname = d.PrettyName
	p.Element = d.Element
	p.Rarity = d.Rarity
	p.Roles = d.Roles
	p.WeaponType = d.WeaponType
	p.Location = "Off-Field"
	if d.Onfield {
		p.Location = "On-Field"
	}
	p.ArtifactRV = fmt.Sprintf("%.1f", r.ArtifactRV)
	p.RVScore = int(r.RVScore * pctToInt)
	p.RVRating = format.ScoreToRating(r.RVScore)
	p.ArtifactCV = fmt.Sprintf("%.1f", r.ArtifactCV)
	p.ArtifactScore = int(r.ArtifactScore * pctToInt)
	p.ArtifactRating = format.ScoreToRating(r.ArtifactScore)
	p.LevelScore = int(r.LevelScore * pctToInt)
	p.LevelRating = format.ScoreToRating(r.LevelScore)
	p.TalentScore = int(r.TalentScore * pctToInt)
	p.TalentRating = format.ScoreToRating(r.TalentScore)
	p.WeaponScore = int(r.WeaponScore * pctToInt)
	p.WeaponRating = format.ScoreToRating(r.WeaponScore)
	p.TotalScore = int(r.TotalRating * pctToInt)
	p.TotalRating = format.ScoreToRating(r.TotalRating)
	p.ConstellationMet = "No"
	p.ConstellationRating = "D"
	if r.ConstellationMet {
		p.ConstellationMet = "Yes"
		p.ConstellationRating = "S"
	}
	p.Comments = strings.Split(strings.TrimSuffix(r.Comments, "\n"), "\n")
	if len(p.Comments) == 1 && p.Comments[0] == "" {
		p.Comments = []string{}
	}
	p.SubRVs = map[string]string{}
	for k, v := range r.SubstatRVs {
		p.SubRVs[format.PrintStat(k)] = fmt.Sprintf("%0.1f", format.Round(v))
	}

	return p
}
