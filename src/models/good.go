package models

// GOOD base.
type IGOOD struct {
	Format     string         `json:"format"`  // A way for people to recognize this format.
	Version    int            `json:"version"` // GOOD API version.
	Source     string         `json:"source"`  // The app that generates this data.
	Characters []Character    `json:"characters"`
	Artifacts  []Artifact     `json:"artifacts"`
	Weapons    []Weapon       `json:"weapons"`
	Materials  map[string]int `json:"materials"`
}

// GOOD artifact.
type Artifact struct {
	SetKey      string      `json:"setKey"`  // e.g. "GladiatorsFinale"
	SlotKey     string      `json:"slotKey"` // e.g. "plume"
	Level       int         `json:"level"`   // 0-20 inclusive
	Rarity      int         `json:"rarity"`  // 1-5 inclusive
	MainStatKey string      `json:"mainStatKey"`
	Location    string      `json:"location"` // where "" means not equipped.
	Lock        bool        `json:"lock"`     // Whether the artifact is locked in game.
	Substats    []Substat   `json:"substats"`
	ID          interface{} `json:"id"` // This is not defined to be a string or an int
}

// GOOD substat.
type Substat struct {
	Key   string  `json:"key"`   // e.g. "critDMG_"
	Value float32 `json:"value"` // e.g. 19.4
}

// GOOD weapon.
type Weapon struct {
	Key        string      `json:"key"`        // e.g. "CrescentPike"
	Level      int         `json:"level"`      // 1-90 inclusive
	Ascension  int         `json:"ascension"`  // 0-6 inclusive. need to disambiguate 80/90 or 80/80
	Refinement int         `json:"refinement"` // 1-5 inclusive
	Location   string      `json:"location"`   // where "" means not equipped.
	Lock       bool        `json:"lock"`       // Whether the weapon is locked in game.
	ID         interface{} `json:"id"`         // This is not defined to be a string or an int
}

// GOOD character.
type Character struct {
	Key           string         `json:"key"`           // e.g. "Rosaria"
	Level         int            `json:"level"`         // 1-90 inclusive
	Constellation int            `json:"constellation"` // 0-6 inclusive
	Ascension     int            `json:"ascension"`     // 0-6 inclusive. need to disambiguate 80/90 or 80/80
	Talent        map[string]int `json:"talent"`        // Talent keys: auto, skill, burst
}
