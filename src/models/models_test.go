// nolint: mnd, goconst,nolintlint
package models

import "testing"

// Quick check to help ensure defaults dont change unintentionally.
func TestModelsGetDefaults(t *testing.T) {
	t.Parallel()
	rating := DefaultRating()
	subs := DefaultSubs()
	mainstat := DefaultMainstats()
	conf := DefaultConfig()
	mod := DefaultModifier()

	if rating.ArtifactCV != 0 || rating.Comments != "" || rating.TotalRating != 0 {
		t.Fail()
	}
	if subs[0] != "atk_" || len(subs) != 7 {
		t.Fail()
	}
	if mainstat["goblet"][0] != "atk_" || mainstat["sands"][0] != "atk_" || mainstat["circlet"][0] != "critDMG_" {
		t.Fail()
	}
	if len(conf.AllowedSets) != 0 || len(conf.AllowedEffects) != 0 || len(conf.TalentPriority) != 3 {
		t.Fail()
	}
	if len(mod.AllowedSets) != 0 || len(mod.AllowedEffects) != 0 || len(mod.TalentPriority) != 3 {
		t.Fail()
	}
}

func TestModelIDTypes(t *testing.T) {
	t.Parallel()

	testArts := []Artifact{
		{
			SetKey:      "GoldenTroupe",
			Rarity:      5,
			Level:       20,
			SlotKey:     "plume",
			MainStatKey: "atk",
			Substats: []Substat{
				{Key: "hp_", Value: 8.2},
				{Key: "critDMG_", Value: 7.8},
				{Key: "critRate_", Value: 9.3},
				{Key: "def_", Value: 10.2},
			},
			Location: "Albedo",
			Lock:     true,
			ID:       "artifact_66",
		},
		{
			SetKey:      "GoldenTroupe",
			Rarity:      5,
			Level:       20,
			SlotKey:     "plume",
			MainStatKey: "atk",
			Substats: []Substat{
				{Key: "enerRech_", Value: 9.1},
				{Key: "hp_", Value: 5.8},
				{Key: "critDMG_", Value: 21},
				{Key: "hp", Value: 568},
			},
			Location: "",
			Lock:     true,
			ID:       67,
		},
		{
			SetKey:      "GoldenTroupe",
			Rarity:      5,
			Level:       20,
			SlotKey:     "plume",
			MainStatKey: "atk",
			Substats: []Substat{
				{Key: "enerRech_", Value: 9.1},
				{Key: "hp_", Value: 5.8},
				{Key: "critDMG_", Value: 21},
				{Key: "hp", Value: 568},
			},
			Location: "",
			Lock:     true,
			ID:       float64(68),
		},
	}
	correctName := []string{
		"artifact_66",
		"67",
		"68",
	}
	for i, art := range testArts {
		if art.GetID() != correctName[i] {
			t.Errorf("Expected artifact ID %s, got %s", correctName[i], art.GetID())
		}
	}

	weapons := []Weapon{
		{
			Key:        "AquaSimulacra",
			Level:      90,
			Ascension:  6,
			Refinement: 1,
			Location:   "Yelan",
			Lock:       false,
			ID:         "weapon_57",
		},
		{
			Key:        "AquaSimulacra",
			Level:      90,
			Ascension:  6,
			Refinement: 1,
			Location:   "Yelan",
			Lock:       false,
			ID:         57,
		},
		{
			Key:        "AquaSimulacra",
			Level:      90,
			Ascension:  6,
			Refinement: 1,
			Location:   "Yelan",
			Lock:       false,
			ID:         float32(57.001),
		},
	}
	correctName = []string{
		"weapon_57",
		"57",
		"57",
	}
	for i, weapon := range weapons {
		if weapon.GetID() != correctName[i] {
			t.Errorf("Expected weapon ID %s, got %s", correctName[i], weapon.GetID())
		}
	}
}
