package models

import (
	"strconv"
)

const (
	AllowedFormat = "GOOD"
	ConfigVersion = "v3.3"
)

type Config struct {
	Version          string                       `json:"version"`
	CharacterConfigs map[string]CharacterConfig   `json:"characters"`
	BuildModifiers   map[string]map[string]string `json:"buildModifiers"` // Character -> weapon -> new builds
	CustomBuilds     map[string]CustomBuild       `json:"customBuilds"`
}

// The recommended build information for the training guide output.
type CharacterConfig struct {
	AllowedSubs        []string            `json:"allowedSubs"`
	AllowedSets        []string            `json:"allowedSets"`
	AllowedEffects     []string            `json:"allowedEffects"`
	AllowedMainstats   map[string][]string `json:"allowedMainstats"`
	RecommendedWeapons []string            `json:"recommendedWeapons"`
	TalentPriority     map[string]int      `json:"talentPriority"`
	// Mainly for characters like sara and faruzan
	RecommendedConstellation int     `json:"recommendedConstellation"`
	RecommendedCV            float32 `json:"recommendedCV"`
}

// Reconfigure build based on equipped weapon.
type CustomBuild struct {
	BuildHelp        string              `json:"buildHelp"` // Hints about how this build is different from standard
	AllowedSubs      []string            `json:"allowedSubs"`
	AllowedSets      []string            `json:"allowedSets"`
	AllowedEffects   []string            `json:"allowedEffects"`
	AllowedMainstats map[string][]string `json:"allowedMainstats"`
	TalentPriority   map[string]int      `json:"talentPriority"`

	RecommendedConstellation int     `json:"recommendedConstellation"`
	RecommendedCV            float32 `json:"recommendedCV"`
}

// Rating of a character.
type CharacterRating struct {
	ArtifactRV       float32            `json:"artifactRV"`
	RVScore          float32            `json:"rvScore"`
	ArtifactCV       float32            `json:"artifactCV"`
	ArtifactScore    float32            `json:"artifactScore"`
	LevelScore       float32            `json:"levelScore"`
	TalentScore      float32            `json:"talentScore"`
	WeaponScore      float32            `json:"weaponScore"`
	ConstellationMet bool               `json:"constellationMet"`
	TotalRating      float32            `json:"totalRating"`
	Comments         string             `json:"comments"`
	SubstatRVs       map[string]float32 `json:"substatRVs"`
}

// Data that doesn't end up in the final rating.
type CharacterIntermediate struct {
	WeaponAtk     float32
	CritRateSum   float32
	CritDamageSum float32
	RecRV         float32
	Sets          map[string]int
	Artifacts     map[string]Artifact
	Weapon        Weapon
	Character     Character
}

type AccountRating struct {
	CharsPerEle     map[string]int
	CharsPerRole    map[string]int
	CharsPerRating  map[string]int
	CharsPerTheater map[string]int
	Comments        string
}

type ID interface{}

// Gives the string interpretation of the ID, which can be a whole number or a string.
func (a Artifact) GetID() string {
	return idToString(a.ID)
}

// Gives the string interpretation of the ID, which can be a whole number or a string.
func (w Weapon) GetID() string {
	return idToString(w.ID)
}

// Takes an ID interface and returns the value as a string.
// If it is read in as a float, it is cast to an int first.
func idToString(id ID) string {
	switch v := id.(type) {
	case string:
		return v
	case int:
		return strconv.Itoa(v)
	case float64:
		return strconv.Itoa(int(v))
	case float32:
		return strconv.Itoa(int(v))
	default:
		return ""
	}
}

func DefaultRating() CharacterRating {
	return CharacterRating{
		ArtifactRV:       0,
		ArtifactCV:       0,
		RVScore:          0,
		ArtifactScore:    0,
		LevelScore:       0,
		TalentScore:      0,
		ConstellationMet: true,
		WeaponScore:      0,
		TotalRating:      0,
		Comments:         "",
		SubstatRVs:       make(map[string]float32),
	}
}

func DefaultSubs() []string {
	return []string{
		"atk_", "def_", "hp_", "eleMas", "critRate_", "critDMG_", "enerRech_",
	}
}

func DefaultMainstats() map[string][]string {
	return map[string][]string{
		"circlet": {
			"critDMG_",
			"critRate_",
		},
		"goblet": {
			"atk_",
		},
		"sands": {
			"atk_",
		},
	}
}

func DefaultConfig() CharacterConfig {
	return CharacterConfig{
		AllowedSubs:        DefaultSubs(),
		AllowedSets:        []string{},
		AllowedEffects:     []string{},
		AllowedMainstats:   DefaultMainstats(),
		RecommendedWeapons: []string{},
		TalentPriority: map[string]int{
			"auto":  6, //nolint: mnd
			"skill": 6, //nolint: mnd
			"burst": 6, //nolint: mnd
		},
		RecommendedConstellation: 0,
		RecommendedCV:            0.0,
	}
}

func DefaultModifier() CustomBuild {
	return CustomBuild{
		BuildHelp:        "",
		AllowedSubs:      DefaultSubs(),
		AllowedEffects:   []string{},
		AllowedSets:      []string{},
		AllowedMainstats: map[string][]string{},
		TalentPriority: map[string]int{
			"auto":  6, //nolint: mnd
			"skill": 6, //nolint: mnd
			"burst": 6, //nolint: mnd
		},
		RecommendedConstellation: 0,
		RecommendedCV:            0,
	}
}
