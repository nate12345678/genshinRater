package readme

import (
	_ "embed"
	"fmt"
	"regexp"
	"strings"

	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"
)

//go:embed template.html
var templ string

const (
	tocStart = "<details><summary>Table of Contents</summary>"
	tocEnd   = "</details>"
)

func ToHTML(md []byte, version string) []byte {
	// Remove coverage badge
	r := regexp.MustCompile(`!\[Coverage\]\(.*\)`)
	md = r.ReplaceAll(md, []byte{})

	// create markdown parser with extensions
	extensions := parser.CommonExtensions | parser.AutoHeadingIDs | parser.NoEmptyLineBeforeBlock
	p := parser.NewWithExtensions(extensions)
	doc := p.Parse(md)

	// create HTML renderer with extensions
	htmlFlags := html.CommonFlags | html.HrefTargetBlank
	opts := html.RendererOptions{Flags: htmlFlags}
	renderer := html.NewRenderer(opts)

	s := string(markdown.Render(doc, renderer))
	s = strings.ReplaceAll(s, "<h1", "</div><div><h1")
	s = strings.ReplaceAll(s, "assets/", "../assets/")
	s = strings.ReplaceAll(s, "<!-- begin toc -->", tocStart)
	s = strings.ReplaceAll(s, "<!-- end toc -->", tocEnd)

	return []byte(fmt.Sprintf(templ, s, version))
}
