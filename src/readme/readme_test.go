package readme

import (
	"strings"
	"testing"

	genshinrater "gitlab.com/nate12345678/genshinRater"
)

func TestReadme(t *testing.T) {
	t.Parallel()
	r := string(ToHTML(genshinrater.Readme, "testing"))
	if !strings.Contains(r, "This page was generated as part of release version testing.") {
		t.Fail()
	}
	if !strings.Contains(r, "<li><a href=\"#usage\">Usage</a>") {
		t.Fail()
	}
	if strings.Contains(r, "![coverage](https://") {
		t.Fail()
	}
}
