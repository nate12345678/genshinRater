// nolint: mnd, nolintlint
package teamcomp

import "gitlab.com/nate12345678/genshinRater/src/data"

// If a filterfunc returns false, a team is not allowed.
type teamFilterFunc func([]string) bool

func getFilterFunc(name string) teamFilterFunc {
	switch name {
	case "Chasca":
		return ChascaFilterFunc
	case "Chevreuse":
		return ChevreuseFilterFunc
	case "Eula":
		return EulaFilterFunc
	case "Mualani":
		return MualaniFilterFunc
	case "Navia":
		return NaviaFilterFunc
	case "Nilou":
		return NilouFilterFunc
	case "Xilonen":
		return XilonenFilterFunc
	}
	return nil
}

func ChascaFilterFunc(chars []string) bool {
	c := 0
	for _, name := range chars {
		if data.Characters[name].Element != data.ElementAnemo &&
			data.Characters[name].Element != data.ElementDendro &&
			data.Characters[name].Element != data.ElementGeo {
			c++
		}
	}
	return c > 2
}

func ChevreuseFilterFunc(chars []string) bool {
	e := 0
	p := 0
	for _, name := range chars {
		if data.Characters[name].Element == data.ElementElectro {
			e++
		}
		if data.Characters[name].Element == data.ElementPyro {
			p++
		}
	}
	return e > 0 && p > 0 && p+e == len(chars)
}

func EulaFilterFunc(chars []string) bool {
	for _, name := range chars {
		if data.Characters[name].Element == data.ElementElectro {
			return true
		}
	}
	return false
}

func MualaniFilterFunc(chars []string) bool {
	for _, name := range chars {
		if data.Characters[name].Element == data.ElementPyro {
			return true
		}
	}
	return false
}

func NaviaFilterFunc(chars []string) bool {
	c := 0
	for _, name := range chars {
		if data.Characters[name].Element != data.ElementAnemo &&
			data.Characters[name].Element != data.ElementDendro &&
			data.Characters[name].Element != data.ElementGeo {
			c++
		}
	}
	return c > 0
}

func NilouFilterFunc(chars []string) bool {
	d := 0
	h := 0
	for _, name := range chars {
		if data.Characters[name].Element == data.ElementDendro {
			d++
		}
		if data.Characters[name].Element == data.ElementHydro {
			h++
		}
	}
	return d > 0 && h > 0 && d+h == len(chars)
}

func XilonenFilterFunc(chars []string) bool {
	c := 0
	for _, name := range chars {
		if data.Characters[name].Element != data.ElementAnemo &&
			data.Characters[name].Element != data.ElementDendro &&
			data.Characters[name].Element != data.ElementGeo {
			c++
		}
	}
	return c > 1
}
