package teamcomp

import (
	"reflect"
	"testing"

	"gitlab.com/nate12345678/genshinRater/src/data"
)

func TestPriorityTeammates(t *testing.T) { //nolint: paralleltest
	data.LoadData()
	data.ReadCompatMatrix()
	tm := GetPriorityTeammates("Albedo", 5)
	if len(tm) != 5 || tm[0] != "AratakiItto" || tm[1] != "Chiori" {
		t.Fail()
	}
}

// Test all filter functions for accuracy.
// If another character needs a filter, just add it to the map.
func TestFilters(t *testing.T) { //nolint: paralleltest
	type filterTestData struct {
		filter     teamFilterFunc
		trueTeams  [][]string
		falseTeams [][]string
	}
	filterMap := map[string]filterTestData{
		"Chasca": {
			filter:     ChascaFilterFunc,
			trueTeams:  [][]string{{"Chasca", "Furina", "Charlotte", "Bennett"}},
			falseTeams: [][]string{{"Chasca", "Xilonen", "Chiori", "Furina"}},
		},
		"Chevreuse": {
			filter:     ChevreuseFilterFunc,
			trueTeams:  [][]string{{"Chevreuse", "Clorinde", "Fischl", "Bennett"}},
			falseTeams: [][]string{{"Chevreuse", "Xilonen", "Chiori", "Furina"}},
		},
		"Eula": {
			filter:     EulaFilterFunc,
			trueTeams:  [][]string{{"Eula", "Furina", "Lisa", "Bennett"}},
			falseTeams: [][]string{{"Eula", "Xilonen", "Chiori", "Furina"}},
		},
		"Mualani": {
			filter:     MualaniFilterFunc,
			trueTeams:  [][]string{{"Mualani", "Furina", "Charlotte", "Bennett"}},
			falseTeams: [][]string{{"Mualani", "Xilonen", "Chiori", "Furina"}},
		},
		"Navia": {
			filter:     NaviaFilterFunc,
			trueTeams:  [][]string{{"Navia", "Furina", "Charlotte", "Bennett"}},
			falseTeams: [][]string{{"Navia", "Xilonen", "Chiori", "Sucrose"}},
		},
		"Nilou": {
			filter:     NilouFilterFunc,
			trueTeams:  [][]string{{"Nilou", "Furina", "Nahida", "Yaoyao"}},
			falseTeams: [][]string{{"Nilou", "Xilonen", "Chiori", "Furina"}},
		},
		"Xilonen": {
			filter:     XilonenFilterFunc,
			trueTeams:  [][]string{{"Xilonen", "Furina", "Charlotte", "Bennett"}},
			falseTeams: [][]string{{"Chasca", "Xilonen", "Chiori", "Furina"}},
		},
	}
	data.LoadData()

	for k, v := range filterMap {
		resFilter := getFilterFunc(k)
		if reflect.ValueOf(resFilter).Pointer() != reflect.ValueOf(v.filter).Pointer() {
			t.Errorf("filter for character %s not correct", k)
		}
		for _, team := range v.trueTeams {
			if !resFilter(team) {
				t.Errorf("team %+v false for %s when it should be true", team, k)
			}
		}
		for _, team := range v.falseTeams {
			if resFilter(team) {
				t.Errorf("team %+v true for %s when it should be false", team, k)
			}
		}
	}
}
