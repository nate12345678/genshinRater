package teamcomp

import (
	"fmt"
	"os"
	"sort"

	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/log"
	"gitlab.com/nate12345678/genshinRater/src/marshal"
)

type TeamChars struct {
	Char1 string
	Char2 string
	Char3 string
	Char4 string
	Score int
}

func (t TeamChars) String() string {
	return fmt.Sprintf("(%s %s %s %s): %d\n", t.Char1, t.Char2, t.Char3, t.Char4, t.Score)
}

const (
	minScore      = 140000 // 5^6 = 15625, 6^6 = 46656, 7^6 = 117,649
	minSurvival   = 7
	skipThreshold = minScore / 100000 // The score a compat value must be greater than for a team to pass.
)

func ComputeTeams(order string) []byte {
	// Get an initial large underlying size to avoid allocations.
	teams := make([]TeamChars, 0, 500)   //nolint: mnd
	filters := make([]teamFilterFunc, 4) //nolint: mnd
	for char1, i := range data.CharacterIndex {
		filters[0] = getFilterFunc(char1)
		for char2, j := range data.CharacterIndex {
			if j <= i || data.CompatMatrix[i][j] <= skipThreshold {
				continue
			}
			filters[1] = getFilterFunc(char2)
			for char3, k := range data.CharacterIndex {
				if k <= j || data.CompatMatrix[i][k] <= skipThreshold || data.CompatMatrix[j][k] <= skipThreshold {
					continue
				}
				filters[2] = getFilterFunc(char3)
				for char4, l := range data.CharacterIndex {
					if l <= k {
						continue
					}
					filters[3] = getFilterFunc(char4)
					skip := false
					for _, f := range filters {
						if f != nil && !f([]string{char1, char2, char3, char4}) {
							skip = true
							break
						}
					}
					if skip {
						continue
					}
					score := data.CompatMatrix[i][j] * data.CompatMatrix[i][k] * data.CompatMatrix[i][l] *
						data.CompatMatrix[j][k] * data.CompatMatrix[j][l] * data.CompatMatrix[k][l]
					survivalScore := data.Characters[char1].SurvivalScore + data.Characters[char2].SurvivalScore +
						data.Characters[char3].SurvivalScore + data.Characters[char4].SurvivalScore
					if score > minScore && survivalScore > minSurvival {
						teams = append(teams, TeamChars{
							Char1: char1,
							Char2: char2,
							Char3: char3,
							Char4: char4,
							Score: score,
						})
					}
				}
			}
		}
	}

	var f func(i, j int) bool
	switch order {
	default:
		fallthrough
	case marshal.OrderAlpha:
		f = func(i, j int) bool {
			return teamAlphaCheck(teams[i], teams[j])
		}
	case marshal.OrderAlphaRev:
		f = func(i, j int) bool {
			return !teamAlphaCheck(teams[i], teams[j])
		}
	case marshal.OrderScore:
		f = func(i, j int) bool {
			return teams[i].Score > teams[j].Score ||
				(teams[i].Score == teams[j].Score && teamAlphaCheck(teams[i], teams[j]))
		}
	case marshal.OrderScoreRev:
		f = func(i, j int) bool {
			return teams[i].Score < teams[j].Score ||
				(teams[i].Score == teams[j].Score && !teamAlphaCheck(teams[i], teams[j]))
		}
	}
	sort.SliceStable(teams, f)

	err := os.WriteFile("teams.txt", []byte(fmt.Sprintf("%v", teams)), os.ModePerm) //nolint: gosec
	if err != nil {
		log.Error(err.Error())
	}
	return []byte(fmt.Sprintf("%v", teams))
}

// check team alpha order from 1-4.
func teamAlphaCheck(a, b TeamChars) bool {
	return a.Char1 < b.Char1 ||
		(a.Char1 == b.Char1 && a.Char2 < b.Char2) ||
		(a.Char1 == b.Char1 && a.Char2 == b.Char2 && a.Char3 < b.Char3) ||
		(a.Char1 == b.Char1 && a.Char2 == b.Char2 && a.Char3 == b.Char3 && a.Char4 < b.Char4)
}

func GetPriorityTeammates(name string, count int) []string {
	chars := make([]string, count)
	idx, ok := data.CharacterIndex[name]
	if !ok {
		return chars
	}
	scores := data.CompatMatrix[idx]
	curScore := 10
	curIdx := 0
	minScore := 4
	for curScore > minScore {
		for i, score := range scores {
			if score == curScore {
				chars[curIdx] = data.IndexedCharacters[i]
				curIdx++
				if curIdx == count {
					return chars
				}
			}
		}
		curScore--
	}
	return chars
}
