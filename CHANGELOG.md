# Changelog
Only feature-level changes are documented here. Minor bug fixes, config changes, or tweaks are not included for brevity.

## v2.19
- All toggle visibility effects now take place without rerunning the rater
- Switched from filter modal to visual filter buttons
- Added additional theater options for 3 primary element combos

## v2.18
- Fixed typing issues with ID fields
- Added rarity backgrounds for character icons
- Improved compatibility with small screens
- Added many filter options to hide characters in output
- Decreased wasm binary size by around 7MiB
- Increased 3-stat S-rating for artifact RV to 2300
- Corrected many incorrect strings in data
- Added some protections against too high levels and ascensions

## v2.17
- Greatly improved dev ergonomics for linux
- Added linter to pipeline
- Moved all links to be relative instead of absolute
- Support for staging server
- Traveler Artifacts now duplicated across all variants

## v2.16
- Added basic accound analysis
- Rater DOM modification done within WASM instead of JS
- Improved division of responsibility between packages

## v2.15
- Cleaned dependencies and function signature of main rater
- Fixed some data inconsistencies and errors

## v2.14
- Removed stats related to weapons from characters
- Added stats that weapons require to the weapon
- Changed prints to leveled logs
- Added changelog changes to release message

## v2.13
- Data and team creation functions for team compositor
- Added synergies to character builds
- Improved visual style of ratings and builds
- Fixed local binary html output

## v2.12
- Enhanced regression testing
- Custom builds shown on builds page
- Removed custom build weapons from default character builds

## v2.11
- Changes role icons on builds to match Imaginarium Theater icons
- Allow for characters to have more than 2 role icons
- Warning when a substat is below 270 RV

## v2.10
- Adds 2pcCombo to any characters who only use a crit rate set and have a sufficiently high base crit rate

## v2.9
- Fixed crash caused by normal attack talent levels higher than 11
- Fixed 'INVALID KEY' being printed what a character doesn't have an artifact equipped in a slot

## v2.8
- Fixed breaking bug caused by items IDed with numbers

## v2.7
- Refactor rating algorithm to be more flexible for future work
- No longer saves config after running locally
- Characters with more than 5 artifacts now have duplicates ignored
- Sorting output by CV

## v2.6
- Added custom talent curves for characters (Electro Traveler, Xilonen)
- Increased internal precision of output values
- Comments about refinements on weapons
- unit testing

## v2.5
- Minor tweaks for v5.1

## v2.4
- Retroactively added this changelog. Some older changes might be incomplete
- Save session data as future defaults
- Enable editing the config on web
- Stabilization of output sorting functions

## v2.3
- Generated HTML readme for webpage
- Character descriptions in config HTML

## v2.2
- Removed Local GUI
- Package changes to make web first-class

## v2.1
- Tweaks and fixes to web

## v2.0
- Created static HTML website using WASM
- Deprecated Local GUI
- Deploy directly to Gitlab Pages
- Split HTML into multi-file static webserver

## v1.9
- Generate config as HTML
## v1.8
- RV of each substat in output

## v1.7
- More data for better scoring of stats

## v1.6
- Character icons in output

## v1.5
- GUI inclusion of sorting

## v1.4
- Precise scoring of weapon level
- Flat subs included as part of RV of percent subs

## v1.3
- Tracking features on the issues page
- Auto-versioning of tool based on tag

## v1.2
- Output sorting

## v1.1
- Added recommended CV function

## v1.0
- Finalized configs for release
- Download new configs from the tool

## v0.6
- First implmentation of GUI
- Refined many of the configs
- Now using Go's templating for extremely robust HTML output

## v0.5
- Removed Markdown in favor of directly printing to HTML
- Better handling of traveler element

## v0.4
- Gitlab pipleline auto release
- Scoring improvements

## v0.3-manual
- More precise scoring of artifacts
- Letter grade scoring

## v0.2-manual
- Configs for every character
- Grid view for HTML output

## v0.1-manual
- Initial release
- Get RV of characters
- Basic rating functionality
- First setup of config file
- Basic output in JSON, Markdown, HTML
