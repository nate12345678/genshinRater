BUILD_DIR ?= build/
WEB_DIR ?= web/

VERSION = $(shell git describe --tags --always)

LDFLAGS = -s -w -X main.Version=$(VERSION)
GOOS ?= windows
GOARCH ?= amd64

CMDS = $(patsubst cmd/%,%,$(wildcard cmd/*))
.PHONY: $(CMDS)
$(CMDS): | $(BUILD_DIR)
	if [ $(shell echo "$@" | grep -c "wasm") != 0 ]; then \
		GOOS=js GOARCH=wasm go build ${BUILDFLAGS} -ldflags="$(LDFLAGS)" -o $(BUILD_DIR) ./cmd/$@ ;\
	else \
		go build ${BUILDFLAGS} -ldflags="$(LDFLAGS)" -o $(BUILD_DIR) ./cmd/$@ ;\
	fi

web-artifacts:
	rm -rf web/*
	mkdir -p web
	cp -r public web/genshinRater
	cp -r assets web/genshinRater/assets
	mkdir -p web/genshinRater/builds
	mkdir -p web/genshinRater/readme
	cp config.html web/genshinRater/builds/index.html
	cp readme.html web/genshinRater/readme/index.html
	cp build/rater.wasm web/genshinRater/

web: rater.wasm html-pages
	mkdir -p web
	cp -r public web/genshinRater
	cp -r assets web/genshinRater/assets
	mkdir -p web/genshinRater/builds
	mkdir -p web/genshinRater/readme
	cp config.html web/genshinRater/builds/index.html
	cp readme.html web/genshinRater/readme/index.html
	cp build/rater.wasm web/genshinRater/

.PHONY: html-pages
html-pages: rater
	./build/rater --html-config
	./build/rater --html-readme

.PHONY: lint
lint:
	golangci-lint run

.PHONY: spellcheck
spellcheck:
	python3 ci/spellcheck.py README.md ci/dictcustom.txt

.PHONY:test
test:
	go test ./... --coverprofile cover.profile
	go tool cover --func cover.profile

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) cover.profile output.json *.html web/*
