package main

import (
	"encoding/json"
	"flag"
	"os"

	genshinrater "gitlab.com/nate12345678/genshinRater"
	"gitlab.com/nate12345678/genshinRater/src/config"
	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/log"
	"gitlab.com/nate12345678/genshinRater/src/marshal"
	"gitlab.com/nate12345678/genshinRater/src/models"
	"gitlab.com/nate12345678/genshinRater/src/rater"
	"gitlab.com/nate12345678/genshinRater/src/readme"
	"gitlab.com/nate12345678/genshinRater/src/teamcomp"
)

var Version = "dev"

func main() {
	showNoArtifacts := false
	getConfig := false
	prettyConfig := false
	prettyReadme := false
	teamComp := false
	outOrder := ""
	filename := "input.json"

	flag.BoolVar(&showNoArtifacts, "show-all", false, "Show all characters in output, not just those with artifacts")
	flag.BoolVar(&getConfig, "get-config", false, "Get an updated config instead of running the rater")
	flag.BoolVar(&prettyConfig, "html-config", false,
		"create an HTML version of the current config instead of running the rater",
	)
	flag.BoolVar(&prettyReadme, "html-readme", false,
		"create an HTML version of the readme instead of running the rater",
	)
	flag.BoolVar(&teamComp, "teams", false,
		"build a team list instead of running the rater",
	)
	flag.StringVar(&outOrder, "output-order", "alpha", "The order to put characters in (alpha, alphaRev, rollValue, rollValueRev, score, scoreRev, critValue, critValueRev)") //nolint: lll
	flag.StringVar(&filename, "file", "input.json", "The relative name of the input file to read")

	flag.Parse()

	log.Infof("Genshin impact build rater %s", Version)
	data.LoadData()

	if getConfig {
		if err := config.FetchConfig(); err != nil {
			log.Error(err.Error())
		}
		return
	}
	if prettyConfig {
		data.ReadCompatMatrix()
		conf, err := config.MakePrettyConfig()
		if err != nil {
			log.Error(err.Error())
		}
		if err := os.WriteFile("config.html", conf, os.ModePerm); err != nil { //nolint: gosec
			log.Error(err.Error())
		}
		return
	}
	if prettyReadme {
		err := os.WriteFile("readme.html", readme.ToHTML(genshinrater.Readme, Version), os.ModePerm) //nolint: gosec
		if err != nil {
			log.Error(err.Error())
		}
		return
	}
	if teamComp {
		data.ReadCompatMatrix()
		teamcomp.ComputeTeams(outOrder)
		return
	}

	input := models.IGOOD{}
	if err := marshal.FromFile(filename, &input); err != nil {
		log.Errorf("failed to read input file: %s", err)
		return
	}

	conf := models.Config{}
	if err := config.GetConfig(&conf); err != nil {
		log.Errorf("failed to read configuration file: %s", err)
		return
	}

	characterRating, err := rater.Run(input, conf)
	if err != nil {
		log.Errorf("failed to score: %w", err)
		return
	}

	for name, rating := range characterRating {
		if !showNoArtifacts && rating.ArtifactScore == 0 {
			delete(characterRating, name)
		}
	}

	outHTML, err := marshal.HTMLPretty(characterRating, outOrder)
	if err != nil {
		log.Errorf("failed to get output html: %w", err)
	}
	outJSON, err := json.MarshalIndent(characterRating, "", "  ")
	if err != nil {
		log.Errorf("failed to get output json: %w", err)
	}

	if err := os.WriteFile("output.json", outJSON, os.ModePerm); err != nil { //nolint: gosec
		log.Errorf("failed to save output json file: %s", err)
	}
	if err := os.WriteFile("output.html", outHTML, os.ModePerm); err != nil { //nolint: gosec
		log.Errorf("failed to save output html file: %s", err)
	}
}
