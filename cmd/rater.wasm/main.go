//go:build js && wasm

package main

import (
	"encoding/json"
	"strings"
	"syscall/js"

	"gitlab.com/nate12345678/genshinRater/src/config"
	"gitlab.com/nate12345678/genshinRater/src/data"
	"gitlab.com/nate12345678/genshinRater/src/log"
	"gitlab.com/nate12345678/genshinRater/src/marshal"
	"gitlab.com/nate12345678/genshinRater/src/models"
	"gitlab.com/nate12345678/genshinRater/src/rater"
	"honnef.co/go/js/dom/v2"
)

var Version = "dev"

var LastRated map[string]models.CharacterRating

func wasmRun() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) any {
		if len(args) != 3 {
			return 1
		}
		outOrder := args[0].String()
		inputJson := args[1].String()
		inConf := args[2].String()

		d := dom.GetWindow().Document()
		resultsElem := d.GetElementByID("results")
		acctElem := d.GetElementByID("acctBox")

		input := models.IGOOD{}
		if err := json.Unmarshal([]byte(inputJson), &input); err != nil {
			log.Warn("failed to read input: " + err.Error())
			resultsElem.SetInnerHTML("<div><p>You need to input your valid character build json!</p></div>")
			return 1
		}

		conf := models.Config{}
		if len(inConf) > 1 {
			if err := json.Unmarshal([]byte(inConf), &conf); err != nil {
				log.Warn("failed to read provided config: " + err.Error())
				resultsElem.SetInnerHTML("<div><p>The provided config is invalid!</p></div>")
				return 1
			}
		} else {
			if err := config.DefaultConfig(&conf); err != nil {
				log.Warn("failed to read config: " + err.Error())
				resultsElem.SetInnerHTML("<div><p>There was an error loading the config. Try reloading the page.</p></div>") //nolint: lll
				return 1
			}
		}

		characterRating, err := rater.Run(input, conf)
		if err != nil {
			log.Warn("failed to rate: " + err.Error())
			resultsElem.SetInnerHTML("<div><p>There was an error scoring your input. Try reloading the page and re-entering your config</p></div>") //nolint: lll
			return 1
		}
		// Score account before deleting character ratings that have no artifacts.
		acct := rater.RateAccount(input.Characters, characterRating)

		html, err := marshal.HTMLPretty(characterRating, outOrder)
		if err != nil {
			log.Warnf("failed to get output html: %s", err)
			resultsElem.SetInnerHTML("<div><p>There was an error scoring your input. Try reloading the page and re-entering your config</p></div>") //nolint: lll
			return 1
		}
		out := strings.Split(strings.Split(string(html), "<section>")[1], "</section>")[0]
		resultsElem.SetInnerHTML("<section>" + out + "</section>")

		acctHTML, err := marshal.HTMLAccount(acct)
		if err != nil {
			log.Warnf("failed to get account output html: %s", err)
			return 1
		}
		acctElem.SetInnerHTML(strings.Split(strings.Split(string(acctHTML), "<body>")[1], "</body>")[0])
		return 0
	})
}

func wasmGetConfig() js.Func {
	return js.FuncOf(func(this js.Value, args []js.Value) any {
		if len(args) != 0 {
			return "incorrect config arguments passed"
		}
		conf := models.Config{}
		if err := config.DefaultConfig(&conf); err != nil {
			log.Warn("failed to read config: " + err.Error())
			return ""
		}
		ret, err := json.Marshal(conf)
		if err != nil {
			log.Warn("failed to marshal config: " + err.Error())
			return ""
		}
		return string(ret)
	})
}

func main() {
	log.Infof("Genshin impact build rater %s", Version)
	data.LoadData()
	js.Global().Set("wasmRun", wasmRun())
	js.Global().Set("wasmGetConfig", wasmGetConfig())
	// Wait forever.
	<-make(chan struct{})
}
