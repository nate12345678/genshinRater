document.addEventListener("DOMContentLoaded", (event) => {
	const checkboxes = document.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkbox.addEventListener('change', function () {
			getBuildFilters();
			filterBuildChars();
		});
	});
	setBuildFilters();
	filterBuildChars();

	const buildSelectors = document.querySelectorAll('.buildSelector');

	buildSelectors.forEach(buildSelector => {
		const container = buildSelector.closest('.charCard');
		const builds = container.querySelectorAll('.data');

		const showSelected = () => {
			const key = buildSelector.value;
			builds.forEach(div => {
				if (div.id != key) {
					div.style.display = 'none';
				} else {
					div.style.display = 'grid';
				}
			});
		}

		buildSelector.addEventListener('change', showSelected);
		showSelected();
	});

});

function getBuildFilters() {
	const checkedStates = {};

	const checkboxes = document.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkedStates[checkbox.id] = checkbox.checked;
	});

	localStorage.setItem("buildFilters", JSON.stringify(checkedStates));
	return checkedStates;
}

function setBuildFilters() {
	const checkedJSON = localStorage.getItem("buildFilters");
	if (checkedJSON == null) {
		return;
	}
	const checkedStates = JSON.parse(checkedJSON);

	const checkboxes = document.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkbox.checked = checkedStates[checkbox.id];
	});

}

function resetBuildFilters() {
	const checkboxes = document.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkbox.checked = true;
	});
	getBuildFilters();
	filterBuildChars();
}

function filterBuildChars() {
	filters = JSON.parse(localStorage.getItem("buildFilters"));
	if (!filters) {
		return;
	}
	cards = document.querySelectorAll('.charCard');
	cards.forEach(card => {
		var shouldHide = false;
		var classNames = Array.from(card.classList).map(cls => cls.toLowerCase());
		for (var filter in filters) {
			if (classNames.includes(filter.toLowerCase()) && !filters[filter]) {
				shouldHide = true;
				break;
			}
		}
		card.style.display = shouldHide ? 'none' : 'block';
	});
}
