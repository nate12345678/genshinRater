if (!WebAssembly.instantiateStreaming) { // polyfill
	WebAssembly.instantiateStreaming = async (resp, importObject) => {
		const source = await (await resp).arrayBuffer();
		return await WebAssembly.instantiate(source, importObject);
	};
}

const go = new Go();
let mod, inst;
var wasmLoaded = WebAssembly.instantiateStreaming(fetch("rater.wasm"), go.importObject).then((result) => {
	mod = result.module;
	inst = result.instance;
	go.run(inst);
}).catch((err) => {
	console.error(err);
});

document.addEventListener("DOMContentLoaded", (event) => {
	let dropbox;
	dropbox = document.getElementById("inputJson");
	dropbox.addEventListener("dragenter", dragenter, false);
	dropbox.addEventListener("dragover", dragover, false);
	dropbox.addEventListener("drop", drop, false);

	showRVCheck = document.getElementById("showSubs")
	showRVCheck.addEventListener("change", setShowRVState);
	showRVCheck.checked = localStorage.getItem("showSubs") == "true";

	if (localStorage.getItem("order")) {
		document.getElementById("outputSelect").value = localStorage.getItem("order");
	}
	hideAcctCheck = document.getElementById("hideAccount")
	hideAcctCheck.addEventListener("change", setAccountVisibility);
	hideAcctCheck.checked = localStorage.getItem("hideAccount") == "true";

	setFilters();
	const checkboxes = document.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkbox.addEventListener('change', function () {
			getFilters();
			filterChars();
		});
	});

	dropbox.value = localStorage.getItem("input");
	wasmLoaded.then(_ => {
		if (dropbox.value != null && dropbox.value != "") {
			run();
		}
	})
});

function dragenter(e) {
	e.stopPropagation();
	e.preventDefault();
}

function dragover(e) {
	e.stopPropagation();
	e.preventDefault();
}

function drop(e) {
	e.stopPropagation();
	e.preventDefault();

	const dt = e.dataTransfer;
	const fileList = dt.files;
	if (fileList[0]) {
		const reader = new FileReader();
		reader.readAsText(fileList[0], "UTF-8");
		reader.onload = (e) => {
			document.getElementById("inputJson").value = e.target.result;
			localStorage.setItem("input", e.target.result);
		};
	}
}

async function run() {
	var order = document.getElementById("outputSelect").value;
	var input = document.getElementById("inputJson").value;

	var conf = localStorage.getItem("config");
	if (conf == null) {
		conf = "";
	}
	localStorage.setItem("order", order);
	localStorage.setItem("input", input);

	var ret = wasmRun(order, input, conf)

	if (ret != 0) {
		document.getElementById("hideAccount").checked = false
	} else {
		setAccountVisibility();
	}

	filterChars();
	setShowRVState();
}

function getFilters() {
	const modal = document.getElementById('filterBox');
	const checkedStates = {};

	const checkboxes = modal.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkedStates[checkbox.id] = checkbox.checked;
	});

	localStorage.setItem("filters", JSON.stringify(checkedStates));
	return checkedStates;
}

function setFilters() {
	const checkedJSON = localStorage.getItem("filters");
	if (checkedJSON == null) {
		return;
	}
	const checkedStates = JSON.parse(checkedJSON);

	const fb = document.getElementById('filterBox');
	const checkboxes = fb.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkbox.checked = checkedStates[checkbox.id];
	});

}

function resetFilters() {
	const checkboxes = document.querySelectorAll('.filterCheck');
	checkboxes.forEach(checkbox => {
		checkbox.checked = true;
	});
	document.getElementById("showAll").checked = false;
	getFilters();
	filterChars();
}

function filterChars() {
	filters = JSON.parse(localStorage.getItem("filters"));
	if (!filters) {
		return;
	}
	cards = document.querySelectorAll('.charCard');
	cards.forEach(card => {
		var shouldHide = false;
		var classNames = Array.from(card.classList).map(cls => cls.toLowerCase());
		for (var filter in filters) {
			if (classNames.includes(filter.toLowerCase()) && !filters[filter]) {
				shouldHide = true;
				break;
			}
		}
		card.style.display = shouldHide ? 'none' : 'block';
	});
}

function confModal() {
	var conf = localStorage.getItem("config");
	if (conf != null) {
		document.getElementById("configJson").value = conf;
	} else {
		document.getElementById("configJson").value = "";
	}
	document.getElementById("confModal").showModal();
}

function closeConfNoSave() {
	document.getElementById("confModal").close();
}

function closeConf() {
	var confTxt = document.getElementById("configJson").value;
	if (confTxt != "") {
		localStorage.setItem("config", confTxt);
	} else {
		localStorage.removeItem("config")
	}
	document.getElementById("confModal").close();
}

function defaultConf() {
	document.getElementById("configJson").value = wasmGetConfig();
}

function setShowRVState() {
	showRVCheck = document.getElementById("showSubs");
	var subs = document.querySelectorAll('.substats');
	var show = showRVCheck.checked
	subs.forEach(sub => {
		sub.style.display = show ? 'block' : 'none';
	});
	localStorage.setItem("showSubs", show);

}

function setAccountVisibility() {
	showAcctCheck = document.getElementById("hideAccount");
	var show = showAcctCheck.checked;
	document.getElementById("acctBox").style.display = show ? 'none' : 'block';
	localStorage.setItem("hideAccount", show);
}
