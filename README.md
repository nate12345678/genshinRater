![Coverage](https://gitlab.com/nate12345678/genshinRater/badges/main/coverage.svg)

# [Genshin Impact Build Rater](https://nate12345678.gitlab.io/genshinRater/) <!-- omit from toc -->

"Like the training guide, but actually helpful"

Quickly rate Genshin builds, calculate useful roll value, and find ways to improve your characters, without needing to know the specifics or going through a complex process.

<!-- begin toc -->

- [What is Genshin Rater?](#what-is-genshin-rater)
- [Usage](#usage)
  - [Options](#options)
  - [Special/Custom Build Definitions](#specialcustom-build-definitions)
  - [Build From Source](#build-from-source)
- [About the Scoring](#about-the-scoring)
  - [Comments](#comments)
  - [Artifact Roll Value \& Crit Value](#artifact-roll-value--crit-value)
  - [Substat RVs](#substat-rvs)
  - [Artifact RV Score](#artifact-rv-score)
  - [Artifact Choice Score](#artifact-choice-score)
  - [Level Score](#level-score)
  - [Talent Score](#talent-score)
  - [Weapon Score](#weapon-score)
  - [Recommended Constellation Met](#recommended-constellation-met)
  - [Total Score](#total-score)
  - [Account Details](#account-details)
- [(WIP) Team Composition Generator](#wip-team-composition-generator)
  - [Survival Score](#survival-score)
  - [How It Works](#how-it-works)
- [Advanced Usage](#advanced-usage)
  - [Configuring `config.json`](#configuring-configjson)
  - [Command Line Flags](#command-line-flags)
- [Future Plans](#future-plans)
- [Contributing](#contributing)
- [About the Character Builds](#about-the-character-builds)
- [FAQ](#faq)
  - [I can't get the rater to work. Help!](#i-cant-get-the-rater-to-work-help)
  - [Why use this instead of the training guide?](#why-use-this-instead-of-the-training-guide)
  - [What about Genshin Optimizer?](#what-about-genshin-optimizer)
  - [A new character came out, but I don't see a build for them!](#a-new-character-came-out-but-i-dont-see-a-build-for-them)
  - [I don't agree with a rating for a character. How do I report this issue/fix it?](#i-dont-agree-with-a-rating-for-a-character-how-do-i-report-this-issuefix-it)
  - [Where does the config data come from?](#where-does-the-config-data-come-from)
  - [Why Gitlab and not GitHub?](#why-gitlab-and-not-github)
- [Changelog](#changelog)
- [Credits](#credits)
- [Disclaimer](#disclaimer)

<!-- end toc -->

# What is Genshin Rater?

As the tagline implies, Genshin Rater is a tool meant to supplement or entirely replace the in-game training guide. it gives detailed information about the quality of a character build, with the aim of providing next steps to improve a character further.

Major features include:
- Rating characters based on their builds
- Feedback on how to improve a character further
- Substat information like [Roll Value and Crit Value](#artifact-roll-value--crit-value)
- Build cards/infographics for every character in the game

# Usage

- Visit https://nate12345678.gitlab.io/genshinRater/
- Scan your account into the GOOD format. [Genshin optimizer](https://frzyc.github.io/genshin-optimizer) also exports in this format.
  - If you don't know what scanner to use, Genshin optimizer also provides a [list](https://frzyc.github.io/genshin-optimizer/#/scanner)
- Copy or drag-and-drop the scan into the input box
- Adjust any settings, press 'Rate Characters'

An example of the output:
![example](assets/exampleResults.png)

## Options
- Output order: The order in which to sort the output of characters
- Show stat roll values: Adds additional rows detailing the exact RV of each substat a character has
- Hide account details: Removes the "Account Details" section
- Character filters: Removes characters from the output based on what is selected

## Special/Custom Build Definitions
Certain characters have multiple, very different major use cases. If such cases exist, there may be an override available. The build is based on the weapon equipped, and if the weapon is not explicitly listed, the character will be scored according to the default build.

If you want to score a character against a custom build but don't have a correct weapon, see [configuring `config.json`](#configuring-configjson).

## Build From Source

- Install go >1.21
- Clone this project
- `go get -u` from project root
- `make` from root or run `windows-build.bat` if you don't have make
  - To run the webserver locally, use `make web` or `local-web.bat`

# About the Scoring

There are 7 score categories per character, each ranging from S to F. The scale is:
- S: 100%
- A: 99% - 88%
- B: 87% - 76%
- C: 75% - 64%
- D: 63% - 52%
- F: 51% - 0%

Recommendations don't disambiguate between high value characters and lower value ones. For example, Arlecchino's normal attack and Lisa's burst are both recommended to be level 10, but in most situations a level on Arlecchino's normal attack would be recommended first.

Just because a character isn't S rated doesn't mean they are bad. S-rated characters should be approaching the absolute peak of what the character is capable of, and is not required to beat any content currently in the game. A party of 8 B- or C-rated characters is capable of completing spiral abyss floor 12, and it is perfectly possible to beat Imaginarium Theatre with several D-rated characters on your roster. F-rated characters are underleveled or poorly built in one or more categories, and should be upgraded before use.

## Comments
Comments are generated as scoring is done, and will provide helpful information on why a score was given. These can range from immediately actionable (artifact is not +20), short-term goals (talent is below recommended), or more long-term (rv is below recommended). Some also simply provide helpful pointers (artifact total rv is low). Properly prioritizing how comments are addressed will allow you to more quickly increase the character's score.

Comments on each character all indicate how scores might be improved, but are not ordered by importance. The relative value of addressing a comment varies heavily based on character, constellations, weapon refinements, etc.

## Artifact Roll Value & Crit Value
Roll Value (RV) and Crit Value (CV) are two stats that are used to determine the quality of a characters artifacts. RV, as calculated using this tool, adds up the total number of rolls into stats that are defined as 'useful' on a per-character basis. The maximum amount of rv a single artifact can have is 900 - 9 rolls at 100% value (initial stats also count as rolls). Some artifacts only have 8 total rolls (3 to start) and most substat rolls are not 100% of the maximum value.

Flat substats (atk, hp, def) are treated as if they were a percentage stat roll. For example, a def substat with a value of 23, on Noelle, who has a base def of 798, would get the RV equivalent of a 2.88% def% substat. Attack rolls additionally account for the base attack of the equipped weapon.

Crit Value is equal to 2 times the crit rate plus the crit damage of substats on a piece - an artifact that has 3.1 crit rate and 7.8 crit damage has a total of 14 CV. This stat can be used to measure how effective artifacts are at enhancing a characters damage, as artifact substats are the primary way to get CV. Not all characters need high crit value; Kokomi notoriously doesn't want any at all. While CV is not accounted for in a character's rating, the RV of crit rolls is still scored on characters that do need the stat.

For a more detailed guide on artifacts and their stats, see [this Keqing Mains guide](https://keqingmains.com/misc/artifacts/).

## Substat RVs
If enabled, the rater will print out the per-substat RV of each recommended stat. These are not scored individually, and are for build examination only.

## Artifact RV Score
This score is the total RV divided by the recommended RV. The recommended RV for characters is based on the number of counted substats they have, and has the following values for 1 to 7 substats, respectively:

`800, 1500, 2300, 2900, 3200, 3500, 3600`

## Artifact Choice Score
The artifact choice score is a compound of 3 different factors:
- The level of the artifacts, where 100% is all +20
- The main stats of the sands, goblet, and circlet, where 100% is all correct stats based on what is defined in the [build card](https://nate12345678.gitlab.io/genshinRater/builds/)
- The set bonuses of the artifacts, where 100% is an appropriate 4-piece set (2-piece sets are each worth 33%)

Artifact level and main stat are weighed more heavily than set bonus.

## Level Score
This score is equal to the percentage of their max level stats a character is. All characters are recommended to be level 90 by this tool. Information about the scaling can be found on [the Fandom wiki](https://genshin-impact.fandom.com/wiki/Character/Level_Scaling).

> Why level 90? Almost every character wants one of 3 things: High base HP or DEF, reaction damage increase, or higher talent levels. All of these are covered by ascending and leveling to 90. Furthermore, while small, the damage a character does to an enemy increases as their level does, regardless of character stats.

## Talent Score
Talents are scored based on the percentage of the recommended talent level's multiplier that the current talent level has. This info can be found on [the Fandom wiki](https://genshin-impact.fandom.com/wiki/Talent#Combat_Talent_Scaling). The weight towards the total talent score is based on the percentage of the sum of talent levels a talent has. Talents above the recommended level provide no additional weight.

In other words, the closer you are to the recommended level, the better, but going over does not increase the score.

Talent levels are recommended roughly as follows:
  - 2: Unused or inconsiquential in regular combat scenarios
  - 6: Used in combat, but provides limited damage or utility
  - 8: Important to character's kit, but increasing the level further provides minimal value
  - 10: Essential part of the character's kit, provides a large portion of the character's value

> Note: I tried to keep the sum of talent levels to 26 or less. It is very rare for triple crowning a character to be a significant upgrade over 10/10/6 or 10/8/8.

Why level unimportant talents? The resin cost to value add of increasing even inconsequential damage can be much higher than trying to get better artifacts.

For example, my Xiangling deals ~29,000 per hit of pyronado at talent level 8, and ~3800 Guoba damage at talent level 5. Leveling guoba to 6 will increase its damage by ~300. Though this only translates to a ~0.5% total damage increase, it cost only 80 resin instead of potentially thousands to find an artifact that would increase Xiangling's damage by the same amount.

Even leveling usually unused talents to 2 can provide value. For less than 10 resin (or just from finding talent books in chests) that talent's stats will be increased by ~8% in value. Though often negligible, the resin cost also is and still makes the character stronger. When using gameplay techniques like normal attack weaving, this can even slightly increase overall team damage.

## Weapon Score
Weapon score is a combination of two factors: the percent of level 90 stats the weapon has and if the weapon is recommended.

This is calculated based on values [here](https://genshin-impact.fandom.com/wiki/Weapon/Level_Scaling). If a character does not care about attack, only the substat value is calculated.

Recommended weapons are all weighted equally. You either have a recommended weapon, or you don't. Favor is not given to limited or five star weapons.

> I recognize that signature weapons often provide a significant power increase over 4 star weapons, but for many players, weapon banner is a poor way to spend their primogems. Incentivizing poor spending habits is not the point of this tool.

Comments will also be generated about the refinement of certain weapons to help guide weapon improvement, but are not scored. The recommendations are generally as follows:
- All 3 star weapons are R5 (except black tassel and dark iron sword).
- Favonius weapons are R3. This is generally the point where you can reliably produce particles within a few normal attacks at 30% Crit Rate.
- Sacrificial weapons are R4 (except sac. jade). This is the point where almost all characters will have the cooldown reset before the next rotation.
- Any weapons where an R5 3 star is a better alternative than a low-refine 4 star (ex. Slingshot vs Rust, Flute of Ezpitzal vs Harbinger of Dawn).

Event weapons should also be R5, but it is currently impossible to obtain them after the event completes, so the comment would not be actionable.

## Recommended Constellation Met
Constellations are binary, and used sparingly on four stars only. Certain characters have a lot of potential locked behind them or are difficult to play without, so this is meant to reflect that. This tool will *never* recommend constellations of limited five star characters.

Character constellations are only recommended if more than ~40% of their total value is locked behind a constellation, or the character is extremely difficult to play without. Notable examples include C2 Kujou Sara and C6 (Madam) Faruzan. 

Traveler variants will always have their highest available constellation recommended.

> This tool is designed with the goal of helping people maximize their resin spending to improve their characters. Constellations are unobtainable by playing the game regularly, and require the use of gacha. It is disingenuous to tell someone that their build is bad simply because they didn't choose to spend more primos on a single character.

## Total Score

Total score is a multiplied factor of all the other scores listed above. This is calculated as:

`character.TotalRating = (1 + character.RVScore) * (1 + character.ArtifactScore) * (1 + character.LevelScore) * (1 + character.TalentScore) * (1 + character.WeaponScore) * (1 + character.ConstellationScore) / 64`

Having a single stat with a very low score will significantly impact the overall rating of a character.

## Account Details

Account details describe the overall state of the account, listing characters by element, role, and rating. This only counts level 70 and above characters, since lower level characters are generally too weak or ineligible for endgame content.

If the provided scan has at least 25 characters level 70 or higher, the rater will also attempt to determine eligibility for Imaginarium Theater 'Hard' and 'Visionary' modes. Since it is unknown what specific characters will be opening cast and which special guest stars will be available (and historical data is useless), eligibility is calculated as three less than the highest minimum (minimum backups, total overlap with opening cast, no special guest stars) and well above the absolute minimum characters required(minimum backups, no overlap with opening cast, all 4 special guest stars), and should fairly accurately represent the ability to complete a theater with the given elements.

# (WIP) Team Composition Generator

> This portion of the tool is pre-release. It may be broken, missing features, or output nonsensical data.

The Team Comp Generator (TCG) is the next step for the rater to provide better feedback about genshin accounts, and help you come up with new team ideas based on the characters you have built.

## Survival Score

The survival score is a rating of a specific character's ability to keep themselves and their team alive at their recommended constellation, on a scale of ~0-10. This score is used to measure how well a typical player might be able to survive with this team against challenging content.

- \>10: Healers who can heal their entire team very quickly from major damage (Jean, Xianyun).
- 7-10: Typical healers or shielders (Bennett, Zhongli).
- 4-6: Either keep themselves alive without assistance (Arlecchino, Clorinde), or provide decent damage mitigation and minor healing (Beidou, Xingqiu).
- 1-3: provide some sort of enemy taunt, or another utility that makes the team somewhat harder to damage.
- 0: Do not affect team survival in any major way.
- <0: High-risk characters who should not dodge during their attacks (Ganyu, Yoimiya), or characters who actively harm their whole team without healing them (Furina).

## How It Works

Since it would be impossible to generate, validate, and score the over 75 million possible character combinations TCG uses a matrix of "character compatibility". For each two characters, give them a score 0-10 based on how likely/well they work together on a team. This matrix is found in `src/data/compatibility.csv` (recommend opening in spreadsheet software like Excel).

These scores are taken and multiplied across all 4 characters on a given team, and if a threshold score is met, the team is considered valid. Eliminating very low scores helps improve program performance and makes sure that only realistic teams are created.

All possible combinations of 4 Characters are iterated through, checking if any characters are totally incompatible (score of 0). If the team is potentially compatible, their survival scores are added to make sure the team has reasonable survival odds, if not, the team is skipped.

Certain characters have additional team requirements, to make sure that they are only put in teams appropriate for their skillset. These characters, and their restrictions, are as follows:

- Chasca: Only Cryo/Electro/Hydro/Pyro teammates
- Chevreuse: Only Electro/Pyro teammates, and at least one Electro teammate
- Mualani: At least one Pyro character on the team
- Navia: At least one Cryo/Electro/Hydro/Pyro teammate
- Nilou: Only Dendro/Hydro teammates, and at least one Dendro teammate
- Xilonen: At least two Cryo/Electro/Hydro/Pyro teammates

# Advanced Usage

All character build settings are listed in `config.json`. Changing this config will result in the next run of the rater to use the new settings. This can be particularly useful if you want to score the RV of an experimental build.

JSON-formatted output is provided for advanced parsing of output when running the tool locally.

## Configuring `config.json`

If you are and advanced users who is familiar with JSON editing and character building, you can change the configuration file to better suit your needs.

Each version of the tool comes with a new `config.json` that contains the most up-to-date build definitions, which can be obtained by pressing the 'Default' button in the config modal on the website. It is recommended to edit the config in an external editor like notepad or VSCode. The contents of the text box will be saved to your browser for future sessions, but new data from a changed default will not be added, including new characters.

Most of the values in `config.json` are self-explanatory. All strings used match those generated by Genshin Optimizer. Of note, the `allowedEffects` field defines what 2-piece set bonuses are allowed.

Build Modifiers contains a map keyed with character names, which in turn contain a map keyed with weapon names. The value of each weapon name corresponds to a custom build, where each build has a unique name.

Adding a weapon to trigger a custom build for a character requires adding the key name of the weapon to the map, and identifying which build should be used.

Helpful information about what data is available, such as weapon and artifact keys, can be found [here](https://gitlab.com/nate12345678/genshinRater/-/tree/main/src/data)

## Command Line Flags

There are some command line flags to change the default behavior of the rater. These can be seen by running `rater.exe -h` in powershell or command prompt from the directory the rater is installed in.

# Future Plans
See the [issues](https://gitlab.com/nate12345678/genshinRater/-/issues) page.

# Contributing

I configured most of the character build recommendations by hand. I likely made mistakes, especially regarding and weapon recommendations. Leave [comments on this issue](https://gitlab.com/nate12345678/genshinRater/-/issues/1) on what characters you think should be changed to help me more quickly find errors.

Just because a build can be put on a character doesn't mean its recommended. For example, the tool suggests only an hp% goblet on zhongli, even though some showcase builds may use geo%. This is aimed at recommending gear that is the most appropriate for the greatest number of players. Please keep this in mind when making suggestions.

# About the Character Builds

A majority of character data in `config.json` is based on my own extensive experience playing Genshin, developing builds, and using community guides. I don't actively partake in any theorycrafting communities, and therefore have some differing opinions about how characters should be built. Most of my build recommendations are "function first" - I won't recommend building a character to maximize their entire kit when 90% of their value comes from a single ability or stat. Furthermore, I don't always agree with what some characters are best suited for. I acknowledge this, and attempt to provide recommendations for both my theories and the wider community's.

That said, My primary source of information on characters I don't have experience with is [Keqing Mains](https://keqingmains.com/). Their body of work spans the entire character roster, and most of the guides are very detailed. They can serve as a good baseline for deciding how to develop your characters.

I also strongly recommend using [Genshin Optimizer](https://frzyc.github.io/genshin-optimizer) to finalize your builds. KQM has a [good guide](https://keqingmains.com/misc/artifacts/#Using_Genshin_Optimizer) on how to get the most out of the tool. Assuming you used it correctly, GO will be able to give more precise pointers on how to improve your artifacts based on what you already have.

# FAQ

## I can't get the rater to work. Help!

See [this Reddit post](https://www.reddit.com/r/GenshinImpactTips/comments/1fykob3/think_the_training_guide_sucks_genshin_impact/). If the comments there don't help, post a question yourself, create a new post and tag me, or send me a DM. I will get back to you on there eventually.

## Why use this instead of the training guide?

This tool has many advantages over the training guide, including:
- More accurately assess the overall quality of a character build
- Knowing what weapons are actually good on a character
- Recommending specific talent levels, not just 10/10/10
- Score character's artifacts by mainstats, sets, and substats, instead of just level

Compared to the in-game artifact tools, this can:
- Recognize actually relevant substats for a given build
- Recommend only the most relevant stats for all characters
- Identify which pieces need the most improvement

## What about [Genshin Optimizer](https://frzyc.github.io/genshin-optimizer)?

Genshin Rater _does not and cannot_ replace Genshin Optimizer(GO); it is intented to compliment it. GO reads currently existing data and tries to optimize against only that information. It does not provide information like "the weapon you are using is bad on this character, you should use a different one". Likewise, the Rater cannot make any optimizations for a character, just tell you what can be improved. You should use Genshin Rater alongside GO, so it can tell you what you can spend your resin on to improve a specific character. After spending the resin, return to GO and come up with a new build, and rate it again until you are satisfied.

Example steps for using the tools together:
1. Examine the [build card](https://nate12345678.gitlab.io/genshinRater/builds/) for a specific character
2. Level up the character and weapon until near the recommended values
3. Assign artifacts using GO
4. Export the GO database into Genshin Rater, score the character
5. Make notes about what the character needs the most improvement in
6. Spend some time gathering materials, artifacts, etc until improvements can be made
7. Repeat 2-6 until satisfied, or another character becomes higher priority

## A new character came out, but I don't see a build for them!

- Wait for a new update. I usually prepopulate new characters and weapons as they are leaked, but that information is often wrong.
- If you are using a custom config, you will have to get a new copy of the default config and edit it with your changes.

## I don't agree with a rating for a character. How do I report this issue/fix it?

- See [Contributing](#contributing)
- To fix it for your rater, see [Configuring `config.json`](#configuring-configjson)

## Where does the config data come from?

- See [Contributing](#contributing) and [About the character builds](#about-the-character-builds)
- [Source?](https://i.imgflip.com/7dbwki.jpg)

## Why Gitlab and not GitHub?

- I use Gitlab more regularly.
- I know how to make CI pipelines for it.

# Changelog

See [`CHANGELOG.md`](https://gitlab.com/nate12345678/genshinRater/-/blob/main/CHANGELOG.md)

# Credits
- [Hoyolab](https://wiki.hoyolab.com/pc/genshin/aggregate/2), for all the icons
- [Genshin Optimizer](https://frzyc.github.io/genshin-optimizer), for generating valid GOOD models, and being the inspiration of this tool

# Disclaimer

This project, the website, and its maintainers are neither affiliated with nor endorsed by HoYoverse.
