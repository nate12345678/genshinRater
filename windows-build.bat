@echo off
set "BUILD_DIR=.\build\"
set "LDFLAGS= -s -w"

for /f %%i in ('git describe --tags --always') do set VERSION=%%i

go build -ldflags="%LDFLAGS% -X main.Version=%VERSION%" -o %BUILD_DIR% .\cmd\rater
